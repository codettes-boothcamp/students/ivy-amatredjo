**Installing ESP32 for Aurdino IDE**

1. In your Arduino IDE, go to File> Preferences
2. Enter https://dl.espressif.com/dl/package_esp32_index.json into the “Additional Board Manager URLs” field as shown in the figure below. Then, click the “OK” button
3. Open the Boards Manager. Go to Tools > Board > Boards Manager…
4. Search for ESP32 and press install button for the “ESP32 by Espressif Systems“
5. That’s it. It should be installed after a few seconds.

![Screenshot](../img/esp32/esp32-4.jpg)

![Screenshot](../img/esp32/esp32-5.jpg)

A fantastic feature of any WiFi-enabled microcontroller like ESP32 is the ability to update its firmware wirelessly. This is known as Over-The-Air (OTA) programming.

**What is OTA programming in ESP32?**

The OTA programming allows updating/uploading a new program to ESP32 using Wi-Fi instead of requiring the user to connect the ESP32 to a computer via USB to perform the update.

OTA functionality is extremely useful in case of no physical access to the ESP module. It helps reduce the amount of time spent for updating each ESP module at the time of maintenance.

One important feature of OTA is that one central location can send an update to multiple ESPs sharing same network.

The only disadvantage is that you have to add an extra code for OTA with every sketch you upload, so that you’re able to use OTA in the next update.

**3 Simple Steps To Use Basic OTA with ESP32**

1. Install Python 2.7.x seriesThe first step is to install Python 2.7.x series in your computer.
2. Upload Basic OTA Firmware SeriallyUpload the sketch containing OTA firmware serially. It’s a mandatory step, so that you’re able to do the next updates/uploads over-the-air.
3. Upload New Sketch Over-The-AirNow, you can upload new sketches to the ESP32 from Arduino IDE over-the-air.

**Upload OTA Routine Serially**

The factory image in ESP32 doesn’t have an OTA Upgrade capability. So, you need to load the OTA firmware on the ESP32 through serial interface first.

It’s a mandatory step to initially update the firmware, so that you’re able to do the next updates/uploads over-the-air.

The ESP32 add-on for the Arduino IDE comes with a OTA library & BasicOTA example. You can access it through **File > Examples > ArduinoOTA > BasicOTA**.

The following code should load. But, before you head for uploading the sketch, you need to make some changes to make it work for you. 
You need to modify the following two variables with your network credentials, so that ESP32 can establish a connection with existing network.

![Screenshot](../img/esp32/esp32-3.jpg)

```
const char* ssid = "......";
const char* password = "......";
```

Once you are done, go ahead and upload the sketch.

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "telew_f21";
const char* password = "2c0f5b91";

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
}
```

Now, open the Serial Monitor at a baud rate of 115200. And press the EN button on ESP32. 
If everything is OK, it will output the dynamic IP address obtained from your router. Note it down.

![Screenshot](../img/esp32/esp32-1.jpg)

Step 3: Upload New Sketch Over-The-Air
Now, let’s upload a new sketch over-the-air.

Remember! you need to add the code for OTA in every sketch you upload. Otherwise, you’ll loose OTA capability and will not be able to do next uploads over-the-air. So, it’s recommended to modify the above code to include your new code.

As an example we will include a simple Blink sketch in the Basic OTA code. Remember to modify the SSID and password variables with your network credentials.

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "telew_f21";
const char* password = "2c0f5b91";

//variabls for blinking an LED with Millis
const int led = 2; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

void setup() {

pinMode(led, OUTPUT);
  
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();  
  
//loop to blink without delay
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
  // save the last time you blinked the LED
  previousMillis = currentMillis;
  // if the LED is off turn it on and vice-versa:
  ledState = not(ledState);
  // set the LED with the ledState of the variable:
  digitalWrite(led,  ledState);
  }

}
```

Note: *In above program, we have not used delay() for blinking an LED, because ESP32 pauses your program during the delay(). If next OTA request is generated while Arduino is paused waiting for the delay() to pass, your program will miss that request.*

Once you copy above sketch to your Arduino IDE, go to Tools > Port option and you should see something like this: esp32-xxxxxx at your_esp_ip_address If you can’t find it, you may need to restart your IDE.

Select the port and click Upload button. Within a few seconds, the new sketch will be uploaded. And you should see the on-board LED blinking.

![Screenshot](../img/esp32/esp32.jpg)

Important:* Use the same BasicOTA sketch, for the blinking LED and choose another board "name" for it. Take one that has a OLED in its name*

![Screenshot](../img/esp32/OLED.jpg)

Note: **Don't press the Boot button too hard, else it will get loose, because it's very delicate. So I have broken mine.**

**Create a webserver with websockets using an ESP32  in Arduino**

Hosting a simple web page, while building it with a WebSocket idea. 
Using the ESP32 to be an access point (AP) and host a web page.
This is how it works:

When a browser requests that page, the ESP32 will serve it. As soon as the page loads, the client will immediately make a WebSocket connection 
back to the ESP32. That allows us to have fast control of our hardware connected to the ESP32.
The WebSocket connection is two-way. Whenever the page loads, it first inquires about the state of the LED from the ESP32. 
If the LED is on, the page will update a circle (fill in red) to reflect that. The circle on the page will be black if the LED is off.
Then, whenever a user presses the “Toggle LED” button, the client will send a WebSocket packet to the ESP32, telling it to toggle the LED.
This packet is followed by another request asking about the LED state so that the client can keep the browser updated with the state of the LED.
I am going to use the led that is on the Arduino. So I don't have to connect a led to my ESP32.

**Install SPIFFS Plugin**

The Serial Peripheral Interface Flash File System, or SPIFFS for short. It's a light-weight file system for microcontrollers with an SPI flash chip.
SPIFFS let's you access the flash memory as if it was a normal file system like the one on your computer (but much simpler of course): 
you can read and write files, create folders ...

We need to use a special program to upload files over SPI.
Go to the [Downloadable Arduino ESP32fs plugin and save the zipfile:](https://github.com/me-no-dev/arduino-esp32fs-plugin) 

**Arduino ESP32 filesystem uploader**
Arduino plugin which packs sketch data folder into SPIFFS filesystem image, and uploads the image to ESP32 flash memory.

**Installation**
1. Make sure you use one of the supported versions of Arduino IDE and have ESP32 core installed.
2. Download the tool archive from [here](https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/tag/1.0)
3. In your Arduino sketchbook directory, create tools directory if it doesn't exist yet.
4. Unpack the tool into tools directory (the path will look like <home_dir>/Arduino/tools/ESP32FS/tool/esp32fs.jar).
5. Restart Arduino IDE.
6. On the OS X create the tools directory in ~/Documents/Arduino/ and unpack the files there

**Usage**
1. Open a sketch (or create a new one and save it). Delete the code from the new sketch and put the code for the webserver. After putting the webserver code. Save it and give it a name "webserver"

Code for the Arduino Webserver
```
#include <WiFi.h>
 #include <SPIFFS.h>
 #include <ESPAsyncWebServer.h>
 #include <WebSocketsServer.h>
  
 // Constants
 const char *ssid = "ESP32-AP";
 const char *password = "letmeinplz";
 const char *msg_toggle_led = "toggleLED";
 const char *msg_get_led = "getLEDState";
 const int dns_port = 53;
 const int http_port = 80;
 const int ws_port = 1337;
 const int led_pin = 16;
  
 // Globals
 AsyncWebServer server(80);
 WebSocketsServer webSocket = WebSocketsServer(1337);
 char msg_buf[10];
 int led_state = 0;
  
 /***********************************************************
  * Functions
  */
  
 // Callback: receiving any WebSocket message
 void onWebSocketEvent(uint8_t client_num,
                       WStype_t type,
                       uint8_t * payload,
                       size_t length) {
  
   // Figure out the type of WebSocket event
   switch(type) {
  
     // Client has disconnected
     case WStype_DISCONNECTED:
       Serial.printf("[%u] Disconnected!\n", client_num);
       break;
  
     // New client has connected
     case WStype_CONNECTED:
       {
         IPAddress ip = webSocket.remoteIP(client_num);
         Serial.printf("[%u] Connection from ", client_num);
         Serial.println(ip.toString());
       }
       break;
  
     // Handle text messages from client
     case WStype_TEXT:
  
       // Print out raw message
       Serial.printf("[%u] Received text: %s\n", client_num, payload);
  
       // Toggle LED
       if ( strcmp((char *)payload, "toggleLED") == 0 ) {
         led_state = led_state ? 0 : 1;
         Serial.printf("Toggling LED to %u\n", led_state);
         digitalWrite(led_pin, led_state);
  
       // Report the state of the LED
       } else if ( strcmp((char *)payload, "getLEDState") == 0 ) {
         sprintf(msg_buf, "%d", led_state);
         Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
         webSocket.sendTXT(client_num, msg_buf);
  
       // Message not recognized
       } else {
         Serial.println("[%u] Message not recognized");
       }
       break;
  
     // For everything else: do nothing
     case WStype_BIN:
     case WStype_ERROR:
     case WStype_FRAGMENT_TEXT_START:
     case WStype_FRAGMENT_BIN_START:
     case WStype_FRAGMENT:
     case WStype_FRAGMENT_FIN:
     default:
       break;
   }
 }
  
 // Callback: send homepage
 void onIndexRequest(AsyncWebServerRequest *request) {
   IPAddress remote_ip = request->client()->remoteIP();
   Serial.println("[" + remote_ip.toString() +
                   "] HTTP GET request of " + request->url());
   request->send(SPIFFS, "/index.html", "text/html");
 }
  
 // Callback: send style sheet
 void onCSSRequest(AsyncWebServerRequest *request) {
   IPAddress remote_ip = request->client()->remoteIP();
   Serial.println("[" + remote_ip.toString() +
                   "] HTTP GET request of " + request->url());
   request->send(SPIFFS, "/style.css", "text/css");
 }
  
 // Callback: send 404 if requested file does not exist
 void onPageNotFound(AsyncWebServerRequest *request) {
   IPAddress remote_ip = request->client()->remoteIP();
   Serial.println("[" + remote_ip.toString() +
                   "] HTTP GET request of " + request->url());
   request->send(404, "text/plain", "Not found");
 }
  
 /***********************************************************
  * Main
  */
  
 void setup() {
   // Init LED and turn off
   pinMode(led_pin, OUTPUT);
   digitalWrite(led_pin, LOW);
  
   // Start Serial port
   Serial.begin(115200);
  
   // Make sure we can read the file system
   if( !SPIFFS.begin()){
     Serial.println("Error mounting SPIFFS");
     while(1);
   }
  
   // Start access point
   WiFi.softAP(ssid, password);
  
   // Print our IP address
   Serial.println();
   Serial.println("AP running");
   Serial.print("AP IP address: ");
   Serial.println(WiFi.softAPIP());
  
   // On HTTP request for root, provide index.html file
   server.on("/", HTTP_GET, onIndexRequest);
  
   // On HTTP request for style sheet, provide style.css
   server.on("/style.css", HTTP_GET, onCSSRequest);
  
   // Handle requests for pages that do not exist
   server.onNotFound(onPageNotFound);
  
   // Start web server
   server.begin();
  
   // Start WebSocket server and assign callback
   webSocket.begin();
   webSocket.onEvent(onWebSocketEvent);
   
 }
  
 void loop() {
   
   // Look for and handle WebSocket data
   webSocket.loop();
 }
 
```
 

2. After saving it go back to Arduino IDE and then go to sketch directory (choose Sketch > Show Sketch Folder). Make sure you see where the sketch folder is. The path looks like this: Pc-> Documents-> Arduino-> webserver folder
3. In the webserver folder create another folder and name it "data". This folder is needed to put the files in it for the filesystem upload.
4. Install Arduino Libraries, Now I need the following libraries to let the webserver and websockets work.

- [AsyncTCP](https://github.com/me-no-dev/AsyncTCP)
- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer)
- [arduinoWebSockets](https://github.com/Links2004/arduinoWebSockets)

5. Now use the same Webserver code and take note of the SSID and password. We will need these to connect our phone/computer to the ESP32’s AP. After editing the code, include all the libraries that we have downloaded. Go to Sketch-> Include Library-> Add Zip library . Do this for all 3 libaries you have downloaded
6. I then uploaded my code to my ESP32 by pressing on the boot button that is on my ESP32.




 

 
 
 
 
 
 
 
 
 


**CAM - Computer Aided Manufacturing**

Computer Aided Manufacturing (CAM) is the use of software and computer-controlled machinery to automate a manufacturing process. 

Based on that definition, you need three components for a CAM system to function:

- Software that tells a machine how to make a product by generating toolpaths.
- Machinery that can turn raw material into a finished product.
- Post Processing that converts toolpaths into a language machines can understand.

**CAD - Computer Aided Design**

Process of creating product models / designs in 2D or 3D
- Keep in mind production tech
- Constraints of both machine & materials
- Communicate design with others

**CAD to CAM Process**
Without CAM, there is no CAD. CAD focuses on the design of a product or part. How it looks, how it functions. CAM focuses on how to make it. 
When a design is complete in CAD, it can then be loaded into CAM. This is traditionally done by exporting a CAD file and then importing it into CAM software. 
Once your CAD model is imported into CAM, the software starts preparing the model for machining. Machining is the controlled process of transforming raw material into 
a defined shape through actions like cutting, drilling, or boring.

CAM software, in our case we use **OPENSCAD**, prepares a model for machining by working through several actions, including:

- Checking if the model has any geometry errors that will impact the manufacturing process.
- Creating a toolpath for the model, which is a set of coordinates the machine will follow during the machining process.
- Setting any required machine parameters including cutting speed, voltage, cut/pierce height, etc…
- Configuring nesting where the CAM system will decide the best orientation for a part to maximize machining efficiency.

Once the model is prepared for machining, all of that information gets sent to a machine to physically produce the part. 
However, we need to speak the machine’s language. 
To do this we convert all of our machining information to a language called G-code. 
This is the set of instructions that controls a machine’s actions including speed, feed rate, coolants, etc.

G-code is easy to read once you understand the format. An example looks like this: 

G01 X1 Y1 F20 T01 S500

This breaks down from left to right as:

- G01 indicates a linear move, based on coordinates X1 and Y1.
- F20 sets a feed rate, which is the distance the machine travels in one spindle revolution.
- T01 tells the machine to use Tool 1, and S500 sets the spindle speed.

*note: there is no Z1, because there is no depth*

Once the G-code is loaded into the machine and we hit start, our job is done. 
Now it’s time to let the machine do the job of executing G-code to transform a raw material block into a finished product.

**OpenSCAD** is the software we use for creating solid 3D CAD objects. It is free software and available for Linux/UNIX, MS Windows and Mac OS X.
OpenSCAD is not an interactive modeller. Instead it is something like a 3D-compiler that reads in a script file that describes the object and 
renders the 3D model from this script file. This gives you (the designer) full control over the modelling process and enables you to easily change 
any step in the modelling process or make designs that are defined by configurable parameters.
OpenSCAD provides two main modelling techniques: First there is constructive solid geometry and second there is extrusion of 2D outlines.

The user interface of OpenSCAD has three parts:

1. The viewing area: Preview and rendering output goes into the viewing area. Using the Show Axes menu entry an indicator for the coordinate axes can be enabled.
2. The console window: Status information, warnings and errors are displayed in the console window.
3. The text editor: The built-in text editor provides basic editing features like text search & replace and also supports syntax highlighting. There are predefined color schemes that can be selected in the Preferences dialog.
 
*Three important things to have in mind:*

1. Know your machine:
- X, Y, Zmax=1/2d
- Fmin-max (Fmax=3K and for wood=1K)
- S for spindle in rpm (Smax= 20K and for wood=8-10K)
- Controller: Grbl, LPT, Serial and USB

2. Which Material to use:
- Thickness
- What type of material: Wood/Plastic

3. The order of operation:
- Inside cut
- Outside cut
- Pocket cut
- Drilling
- profile milling: The rough cut (Flat drilling 1mm) and the fine or detail cut (Final cut) with a ball nose (3-4mm)


OpenSCAD *Libraries / Vitamins*

Vitamins are ready to use libraries of all sorts or components.
Simply link library to your OpenSCAD environment and start using.

[Downloadable OpenSCADlibraries here:](https://github.com/nophead/NopSCADlib)

Git clone into:
Library Locations for:

- Windows: My Documents\OpenSCAD\libraries
- Linux: $HOME/.local/share/OpenSCAD/libraries
- Mac OS X: $HOME/Documents/OpenSCAD/libraries

Example how to chose a few components from the Vitamin Library and align them:

```
include <NopSCADlib/vitamins/fuseholder.scad>
include <NopSCADlib/vitamins/geared_steppers.scad>

//fuse holder
translate([50, 0, 0])fuseholder(20);

//geared_stepper();
rotate([-90,0,0])geared_stepper(28BYJ_48);
```

**CAM -  for CNC milling/cutting**

The making of a Speaker back panel as a 3D design

```
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(10,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);
    
 }

```

**CAM -  Create 2D projections**
The 3D design, can't be exported to a 2D design. So we are adding the projection module and changing the height of the cilinder in the code like this:
Because the pocket does not show? We took a higher Point of projection (eg -6mm lower on z-axis and select cross-section with “cut=true”)

```
projection(cut=true) translate([0,0,-6]){ 
		// your 3D object here}

```

```
projection(){
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(4,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);
    
 }
}

```

After designing, I could export it to CAM as a SVG file.
Click on File-> Export-> Export as SVG

**CAM -  for 3D printing**

[Download](https://ultimaker.com/software/ultimaker-cura) and install Cura
- Import STL (drag/drop)
- Resize and place parts
- Select printer, printer filament material & print settings
- Slice & Save gcode file to SD card

**Production -  3D print Anycubic 4Max Pro**

- Insert SD card on left side (upside down)
- Start printer
- Select “print” scroll to file of print
- Print and wait (printer will heat up first before starting)

Note: If printing ABS close upper lid but for PLA leave it off.

**CAM -  for CNC milling/cutting**

Aspects:

- CNC Machine, Controller, constraints
- Material (wood/plastic/steel)
- Operations / Type (cut-out inner/outer, pocket,drilling)
- Tools/Mills/Toolchange

For each CNC/Material/Operation create table with:

- Tool type, diameter  (D-mm),
- Feedrate (F - mm/min), Plunge Rate (mm/min),
- Spindle RPM (S - rpm),
- Pass-depth/step down (Z mm), Overlap if applicable (%)

Import the 2D design from OpenScad and import the SVG file into the CAM. We worked online with [MakerCam](https://www.makercam.com/).

In MakerCAM we will generate the GCode, but first:

- enable flash in browser
- Right click on the page and click -> Inspect Element
- Select HTML Element: Body ->Content -> Container
- Change CSS “height:100%” to “height:100vh” )
- After that Click on File-> Open SVG file

After opening the file, put the exact parameters for the productions.For this click on each part before editing the parameters.
Click on CAM and then on pocket operation.
Change the Pocket parameters:

- tool diamter(mm): 6
- target depth(mm): -4
- safety height(mm): 10
- stock surface(mm): 0
- step over(%): 40
- step down(mm): 3
- roughing clearness(mm): 0
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Electrohole parameters:

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 3
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Basreflex parameters:

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 10
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Outside cut parameters:


tool diamter(mm): 6
target depth(mm): -9
inside/outside cut: Outside
safety height(mm): 10
stock surface(mm): 0
step down(mm): 3
feedrate(mm/minute): 1000
plunge rate (mm/minute): 500
direction: counter Clockwise

After editing all the parameters, Click on CAM-> And then click Calculate all.
Be sure to check the toolpath by Clicking on Toolpath-> And see if all part names is there that you selected. 
If everything is there Click on CAM-> And then click on Export Gcode
Select every Toolpath before exporting Gcode. After selecting all toolpath click on *Export selected Toolpath*
Save the Gcode and gave it a name. This Gcode will be needed to cut the 2D design on a CNC machine.

**CNC(Computer Numerical Control)** is the automated control of machining tools (such as drills, boring tools, lathes) and 3D printers by means of a computer.
A CNC machine processes a piece of material (metal, plastic, wood, ceramic, or composite) to meet specifications by following a coded programmed instruction 
and without a manual operator. The basic CNC machining process includes the following stages:

- Designing the CAD model
- Converting the CAD file to a CNC program
- Preparing the CNC machine
- Executing the machining operation

**CAD Model Design**
The CNC machining process begins with the creation of a 2D vector or 3D solid part CAD design either in-house or by a CAD/CAM design service company.
Computer-aided design (CAD) software allows designers and manufacturers to produce a model or rendering of their parts and products along with the necessary 
technical specifications, such as dimensions and geometries, for producing the part or product.
And that is what I did. Designing in OpenScad and then Generate the Gcode in MakerCam.

**Machine Setup**
Before the operator runs the CNC program, they must prepare the CNC machine for operation.
These preparations include affixing the workpiece directly into the machine, onto machinery spindles, or into machine vises or similar workholding devices, and attaching the required tooling, such as drill bits and end mills, to the proper machine components.
Once the machine is fully set up, the operator can run the CNC program.
So we loaded the Gcode to the CNC machine to cut the design.
Always use a vacuum cleaner to clean all the dust.

**CAM -  LaserWEB4 / CNCWeb**

LaserWeb / CNCWeb is an application for:

generating GCODE from DXF/SVG/BITMAP/JPG/PNG files for Lasers and CNC Mills (= CAM Operations) and
controlling a connected CNC / Laser machine (running one of the supported firmwares)

[Install laserWeb](https://laserweb.yurl.ch/).

*Laser cutting* is a type of thermal separation process. The laser beam hits the surface of the material and heats it so strongly that it melts or 
completely vaporizes. Once the laser beam has completely penetrated the material at one point, the actual cutting process begins. 
The laser system follows the selected geometry and separates the material in the process.
Depending on the application, the use of process gases can positively influence the result.

The tooldiameter for laser cutting is 0.02 mm machine. Here we need a DKF/SVG file.

The Laser Etching Process
Laser etching, which is a subset of laser engraving, occurs when the heat from the beam causes the surface of the material to melt.

The laser beam uses high heat to melt the surface of the material.
The melted material expands and causes a raised mark.
Unlike with engraving, the depth in etching is typically no more than 0.001”.

Here we need a DNG/BMP file
The Laser Engraving Process is a process where the laser beam physically removes the surface of the material to expose a cavity that reveals 
an image at eye level.

The laser creates high heat during the engraving process, which essentially causes the material to vaporize.
It’s a quick process, as the material is vaporized with each pulse.
This creates a cavity in the surface that is noticeable to the eye and touch.
To form deeper marks with the laser engraver, repeat with several passes.

Here we need SVG/DXF file
**CNC operation VS Laser**

*CNC operations:*

- Outside cut
- Inside cut
- Pocket- Drilling
- The tooldiamter here is 3-6 mm

*Laser:*

- Cutting: Single or Multipass
- Etching: mostly use for photos (PWM power:20-80%\white-Black)
- Engraving: for names (S:1-255% and Q:0.01-1 % in fraction)
- The tooldiameter here is 0.02 mm
- Use wavelength (golflengte)

**Laser Cutting**

LaserWeb/CNCWeb for laser cutting.

As we open the LaserWeb program, we got to see the workspace/Documents/Gcode options on the Files Tab. To place a image on the workspace click on Add Document on the Documents section and select your image that you want  to cut.
After selecting the image, drag the image to the GCode section.
Place all the right parameters after dragging the image.
or you can exported a Gcode in MakerCam and Saved it as SVG file. In the LaserWeb program add the SVG file.
Click on the Setings tab-> There are the options for adding the exact parameters for the Laser cut as below.

*Machine* Settings
Dimensions:

Machine Weight: 600 mm
Machine Height: 840 mm

Origin offset
Show machine: OFF

Machine left Y: 0 mm
Machine buttom X: 0 mm

Tool Head:

Beam: 0.2 mm

Probe tool:

X/Y Probe OFFset: 0 mm
Z Probe OFFset: 1.7 mm

Machine Z stage: OFF
Machine A stage: OFF
Air assit: ON

*File* Setting

SVG:
PX PER INCH: 96
BITMAP DPI: 300

*GCode* Settings

GCode Generator set on Default
GCODE END: M5
TOOL ON: M10
TOOL OFF: M11
LASER INTENSITY: Q

After adding all the parameters I made a New profile and named it: Stepcraft840-laser. And the I applied predefined machine profile
Drag the SVG file to the Gcode section. Select Laser cut.And gave it a name. Mine is Lasercut1. Change the laser poer to 100%, the passes to 1 and the cut Rate to 1500 mm/min
And then generate the GCode
After generating the Gcode. Export the Gcode file and gave it a name:"Lasercut1"
Load the Gcode on the lasercut machine

**Production -  Stepcraft CNC**
StepCraft with UCCNC via UC100

Steps to be taken:

1. Place and secure stock on CNC
2. Load GCode file into UCCNC
3. Set Workpiece zeros (X,Y zero)
4. Simulate to see if gcode will fit on stock by Jogging
5. Set Z- zero
6. Start spindle
7. Set Feed rate at 50%
8. Start Cycle on UCCNC
9. Gradually increase or decrease Feedrate%

If everything is set good. The design can be cut
















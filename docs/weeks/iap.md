## Configuring the RaspberryPi

**WEEK 6: What is Raspberry Pi?**

Raspberry Pi is a microcomputer which has modest features as a computer and is heavily used by hobbyists, 
students and developers across the world. 

![Screenshot](../img/week6/RaspberryPi2.jpg)

*Raspberry Pi 2 features and specifications*

1. CPU: Broadcom BCM2836 900MHz quad-core ARM Cortex-A7 processor
2. RAM: 1 GB SDRAM
3. USB Ports: 4 USB 2.0 ports  
4. Network: 10/100 Mbit/s Ethernet 
5. Power Ratings: 600 mA (3.0 W) 
6. Power Source: 5V Micro USB 
7. Size: 85.60 mm × 56.5 mm 
8. Weight: 45 g 

*Configuring your SD-Card for your RaspberryPi*

Insert a SD-Card in your SD-Card reader slot on your laptop and open win32diskmanager and look for the **Jessie-image** on your laptop, which we will  use to configer the SD-Card.
This will write the image into the SD-Card.

*Note: Whatever you do DON'T format the SD-Card if it prompt up on your laptop*

![Screenshot](../img/week6/diskImager22120.jpg)

![Screenshot](../img/week6/Jessie22120.jpg)

2 important things  to check after configuring the SD Card with your PC.
After configuring the SD Card, you will find a Boot-drive on your PC. Open this and then,
*Open cmdline.txt in notepad ++ and then I hardcoded my adress of my RspberryPi=192.168.1.162.*
This is needed to install or work with putty. Go to your SD-card drive and make a new text file, name it SSh, without the text extention.
To allow putty to access the Raspverry Pi remotely.

![Screenshot](../img/week6/5SD22120.jpg)

![Screenshot](../img/week6/cmdLine22120.jpg)

![Screenshot](../img/week6/RPISD22120.jpg)

**The crosscable**

crossing over:
Allowing me to connect to a computer thinking it connects to a hub or switch. Because we cross the cables
This cable is needed for connecting to a network.

![Screenshot](../img/week6/CrossCable22120.jpg)

*Note*: At home we are using a direct cable for connecting to the internet via the switch or hub.

Making some connection with a network configer the Ethernet Settings on your laptop.

**Ethernet Settings:**

![Screenshot](../img/week6/Ethernet22120.jpg)

![Screenshot](../img/week6/TCPIP22120.jpg)

![Screenshot](../img/week6/IPAdress22120.jpg)

After configuring the Ethernet settings with the ip-adress: 192.168.1.172, plug your crosscable to your raspberryPi.
And give it power by using your USB-cable.
If it works, your plug where the ethernet cable is connected should give you lights-indicators. If not, there is something wrong with the ethernet cable.


*Note: Put your SD-Card in your RaspberryPi!!!*

If it's working, you can see the ip-adress in the command-prompt.

*Putty is a tool to establish a connection with the raspberryPi*

![Screenshot](../img/week6/1Putty22120.jpg)

Start your Putty.exe and login with your username and password.

![Screenshot](../img/week6/2Putty22120.jpg)

![Screenshot](../img/week6/LoginPutty22120.jpg)

Tick: sudo raspi-config **(for superior access)** on your console and press enter. After getting the window on your screen
you can configure the raspberryPi, enabling the options you will find in that window.

*Configure your RaspberryPi*

![Screenshot](../img/week6/1RaspPi22120.jpg)

![Screenshot](../img/week6/2RaspPi22120.jpg)

![Screenshot](../img/week6/3RaspPi22120.jpg)

**Installing Nodjes and Python on our RaspberryPi**

**Ad1: Nodejs**

**Node.js** is an open-source server and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!
And it's free.
Node.js runs the V8 **JavaScript** engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.
A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.
When Node.js needs to perform an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back.
This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.
*Node.js runs on various platforms (Windows, Linus, Unix, Mac OS X, etc.)*
*JavaScript is thegramming Language for the web, it can update and change both HTML and CSS. It can calculate, manipulate and validate data.*

**Express** is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

Some of the Basic Linus Commands are:

1. cd : to go to a directory
2. cd ..: to go back to your last directory
3. touch: to create or update a file
4. mkdir: to cfreate a Directory
5. ls: To see whats in your directory (folder)
6. rm: To remove a file
7. rmdir: To remove a folder

With these commands we can config the raspberryPi *headless*, that's mean that we can install files and make folders without using a keyboard or a monitor

**Nodejs Folder Structure / Frames**


- *nodejs*
-  *projects*
-  *project1:*
- -    index.js
-  *public*
-  -   index.html
-  -   *css*
- - -     style.css
-  package.json
	
*Note*:

Nodejs and Python are two different Website servers
To check our version on our RaspberryPi we use: Nodejs __version , python __version , pip __version
We use the **npm** to download your nodejs dependency (libaries) and for python we use **pip v. 1.5.6**
**Express** is the app for Nodejs and **Flask** is the app for python. 
When using these apps, modules and libaries are running in the background.
Modules are showing us the right path, which lies in the public folder (the route) for nodjes and the templates folder for python.

*JavaScript is the Programming Language for the Web*, it can update and change both HTML and CSS (installfile sheets) and it can calculate, manipulate and validate data.

*And the language we are using for Python is python.*

Installing Nodejs Server:

Open putty and type in the following commands:

1. node --version (to see the version of nodejs first) 
2. curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs (to install the right version of nodejs)
3. go to index.js open it and edit it

```
/ index.js

/**
 * Required External Modules
 */

/**
 * App Variables
 */

/**
 *  App Configuration
 */

/**
 * Routes Definitions
 */

/**
 * Server Activation
 */

```

- go to the project1 folder and type *npm space init space -y* 
- *npm i D nodemon* (to install nodemon) 
- integrate npm init -y 
- you should have your structure in your index.js after you did that you have to put code in it 
- *npm i express* (i means install) 
- open package.json and change "test..." to " "dev": "nodemon ./index.js" 

**NPM stands for Nodes Package Manager**

sudo npm install means it looks for everything in your package.json using the supior rights (it's like admin rights)

```
{
  "name": "project1",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "nodemon ./index.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "nodemon": "^2.0.2"
  },
  "dependencies": {
    "express": "^4.17.1"
  }
}

```
All npm packages contain a file, usually in the project root, called package.json - this file holds various metadata relevant to the project. 
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies. 
It can also contain other metadata such as a project description, the version of the project in a particular distribution, license information, even configuration data - all of which can be vital to both npm and to the end users of the package. 
The package.json file is normally located at the root directory of a Node.js project.


Open index.js and edit some more:

```
// index.js

/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT ||"8000";

/**
 *  App Configuration
 */


app.use(express.static(path.join(__dirname, "public")));

/**
 * Routes Definitions
 */



/**
 * Server Activation
 */

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});

```

Go to your public folder and open the index. html file and type these basic html-codes:

```
<
html>
<body>
<h1>My website</h1>
</body>
</html>

```
To start the nodejs Server tick in : *npm run dev* on your putty console and it starts your HTML.

Open your browser and type in: http://192.168.1.162:8000/ and you can see your webpage using port 8000.

*note*: 

nodemon is the nodejs module to run the server automatically and it runs it in the background

**Ad2: Python, version 2.7.9**

**Python** is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.

**Flask** is a lightweight WSGI web application framework. It is designed to make getting started quick and easy, with the ability to scale up to complex applications. It began as a simple wrapper around Werkzeug and Jinja and has become one of the most popular Python web application frameworks.


**Python Folder Structure / Frames**

- *python*
- -	index.py
- -	static
- - -	*img*
- - *templates*
- - -	index.html
			
After making the folders and files, we open index.py and write the following codes:

```
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

```
Explanation:
@app.route('/'): this determines the entry point; the / means the root of my website, so http://192.168.1.162:5000/
def index(): this is the name you give to the route; this one is called index, because it’s the index (or home page) of the website
which return my web page, when the user goes to this URL.
When port is not given use port:8000

![Screenshot](../img/week9/indexpy_website.jpg)

Installing the Python Server:

1. Install python 2.79 
2. Install flask using the command: sudo pip install flask 
3. Go to your templates folder and open the index. html file and use some basic html-codes, javascript and css to make your HTML page.

So to run the Python Server tick in: *python index.py* on your putty console

# Arduino Uno Serial Communication

A Serial communication is a connection between 2 devices over a cable.
In this case we are using The Arduino uno and the RaspberryPi.

Buildig a simple circuit with 1 LED:

![Screenshot](../img/week7/arduinoled31120.jpg)

using the following code to turn on the LED:

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
**Now we make an serial connection with our RaspberryPi:**

![Screenshot](../img/week7/rasparduino31120.jpg)
	
Structure folder for this project was:

pythonprojects
	Serialcom
		index.py 



open your arduino ide go to file->examples->communinication->physicalpixel 
We copy the code and paste it in the index.py file and edit this for our circuit:

![Screenshot](../img/week9/physicalpixel.jpeg)

```
const int ledPin = 13; // the pin that the LED is attached to
int incomingByte;      // a variable to read incoming serial data into

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = Serial.read();
    // if it's a capital H (ASCII 72), turn on the LED:
    if (incomingByte == 'H') {
      digitalWrite(ledPin, HIGH);
    }
    // if it's an L (ASCII 76) turn off the LED:
    if (incomingByte == 'L') {
      digitalWrite(ledPin, LOW);
    })
  }
}
``` 
**Installing the Arduinno uno to our RaspberryPi:**

1. Open putty and type: sudo pip install pyserial (we need to have the serial libary)
2. connect your Arduino Uno to the RaspberryPi
3. type lsub to see a list of your usbports and on which your Arduino UNO is connected
4. type in: dmesg | grep tty to see the serial port for your RaspberryPi(*/dev/ttyAMA0*  (for raspberry pi 2))
5. Edit your index.py using the following code:

```
import serial
import time
# Define the serial port and baud rate.
# Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager

ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
def led_on_off():
    user_input = input("\n Type on / off / quit : ")
    if user_input =="on":
        print("LED is on...")
        time.sleep(0.1) 
        ser.write(b'H') 
        led_on_off()
    elif user_input =="off":
        print("LED is off...")
        time.sleep(0.1)
        ser.write(b'L')
        led_on_off()
    elif user_input =="quit" or user_input == "q":
        print("Program Exiting")
        time.sleep(0.1)
        ser.write(b'L')
        ser.close()
    else:
        print("Invalid input. Type on / off / quit.")
        led_on_off()

time.sleep(2) # wait for the serial connection to initialize

led_on_off()



```
Go to your pythonprojectsfolder and type sudo pyton index.py

Explaining the Code in the index.py:

As we have seen we first import the libaries we need for python and  that was the *serial* and *time*libaries.
The Serial libary was needed to make the serial connection between the 2 devices and the time labary was needed for the delay function in this code.
Further we devine the serial port and its bound rate, which we have put it to 9600 in this code.
We make a function named: *led_on_off* which we will call to turn the LED on or off. We have a user input to show on the screen with the text "\n Type on / off / quit : "
Then we use a *If then Else statement* to response to the user input.If it its put to "On" then we get the text that the LED is "on". Else we get the text "off". If the LED is put to off after a delay of 1 sec again.
The Arduino then sent a signal to the RaspberryPi to turn the LED On by converting it to b (bytes) for the "H". And "L" for off. 

#Week 8 HTML5, CSS3 and JavaScript

**HTML** 

![Screenshot](../img/week7/html5.png)

HTML5 is the latest evolution of the standard that defines HTML. The term represents two different concepts. It is a new version of the language HTML, with new elements, attributes, and behaviors, and a larger set of technologies that allows the building of more diverse and powerful Web sites and applications. This set is sometimes called HTML5 & friends and often shortened to just HTML5.

Designed to be usable by all Open Web developers, this reference page links to numerous resources about HTML5 technologies, classified into several groups based on their function.

1. Semantics: allowing you to describe more precisely what your content is.
2. Connectivity: allowing you to communicate with the server in new and innovative ways.
3. Offline and storage: allowing webpages to store data on the client-side locally and operate offline more efficiently.
4. Multimedia: making video and audio first-class citizens in the Open Web.
5. 2D/3D graphics and effects: allowing a much more diverse range of presentation options.
6. Performance and integration: providing greater speed optimization and better usage of computer hardware.
7. Device access: allowing for the usage of various input and output devices.
8. Styling: letting authors write more sophisticated themes.

HTML is a scripting language, (Instruction tag)
Beginn with <> and closed by </>

Some basic  tags:
```
1. <B>FOR BOLD</B>
2. <H1> FOR HEADING1</H1>
3. <BR> FOR BREAK (go to the next line)
4. <img src="img/iot.jpg"/>
```
NEW Semantic TAGS that actually have more meaning to the standard 
```
<DIV> tags for layout.
For layout : <article>, <header>, <footer>, <nav>, <aside> 
For widgets: <meter>, <progress> etc
```
Working with HTML5 we have a certain structure: 
The root folder is the public folder

**Public**

- index.html
- **img**
- **css**
- **fonts**
- **js**
- - lib
		
HTML5 websites have the same structure:

1. Site Map
2. Folder Structure
3. Collect Context
4. Look & Feel/Layout
5. Mock up
6. Target Devices
7. Supported Browsers

HTML5 has an Old way of writing:

```
<div id="footer"> </div>
And a
New way of writing:
<Footer> </Footer>

Other semantic tags are:
<Header>
<Article>
<Aside>
<img scr="img/logo.png">
<nav>
<ul>
<li><a
```
**Create basic HTML webpage**

Using the following code:

```
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <title>Title MyPage (shows in browser window)</title>
</head>
<body>
    <!-- body content starts here -->
    Here is the visible Content of the page......
</body>
</html>
```
**Create HTML webpage Layout with Divs and Sections**

using the following code:

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Forest deforstation</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
</head>

<body>
    <header Class = "mainHeader">
        <img src="img/logo.png">
        <nav>
            <ul>
                <li><a href="#" Class="active">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
    </header>

    <div Class="mainContent">
        <div Class="content">
            <article class="articleContent">
                <header>
                    <h2>First Article #1</h2>
                </header>

                <footer>
                    <p Class="post-info">Written by Super Woman</p>                 
                </footer>
                <content>
                    <p> Deforestation is the permanent removal of trees to make room for something besides forest. 
                       This can include clearing the land for agriculture or grazing, or using the timber for fuel, construction or manufacturing. </p>
                </content>
            </article> 

            <article Class="articleContent">
                <header>
                    <h2>2nd Article #2</h2>
                </header>

                <footer>
                    <p class="post-info">Written by Super Woman</p>                 
                </footer>

                <content>
                    <p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
                </content>
            </article> 

       </div>
    </div>

    <aside Class="top-sidebar">
        <article>
            <h2>Top sidebar</h2>
            <p>Lorum ipsum dolorum on top</p>
        </article>
    </aside>

    <aside Class="middle-sidebar">
        <article>
            <h2>Middle sidebar</h2>
            <p>Lorum ipsum doloru in the middle</p>
        </article>
    </aside>

    <aside Class="bottom-sidebar">
        <article>
            <h2>Bottom sidebar</h2>
            <p>Lorum ipsum dolorum at the bottom</p>
        </article>
    </aside>

    <footer Class="mainFooter">
        <p>Copyright &copy; <a href="#" title = " 
 MyDesign">mywebsite.com</a></p>
    </footer>

</body>

</html> 

```



**CSS** is for the look and style!

CSS3 or Cascading Style Sheets enables the separation of presentation and content, including layout, colors and fonts. 
To improve content accessibility, more flexibility and control of presentation characteristics, web pages share formatting by specifying the relevant CSS in a separate .css file

We have:

The *tag* on one side and the *selector* on the other side, 
some of these tags and selectors are:
```
1. <B attributes>
2. <H2 Color="blue">
3. <Section>
4. <article>
```
```
class="postinfo"

.postinfo{
		}
		
id=:"specialtag"

#specialtag{
			}
```
Before you can style your website,
Create the style/style.css file in your root folder, then use the following code:
```
/* 
    Stylesheet for:
    HTML5-CSS3 Responsive page
*/

/* body default styling */

body {
    /* border: 5px solid red; */
    background-image: url(img/bg.png);
    background-color: #87bac4;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}


/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #87bac4;
    color: #FFF;
}


/* define mainHeader image and navigation */
.mainHeader img {
    width: 18%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 18%;
    margin: 1.5% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 18%;
    margin: 1.% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 18%;
    margin: 1.5% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #d99090;
    background-color:#666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
}
```
And then *link the style-sheet* like this:

```
<link href="css/style.css" rel="stylesheet" />
```

![Screenshot](../img/week9/inspectstylecss.jpg)

Page responsiveness to your website

To build in page responsiveness, meaning the page will be displayed different for each device/screen dimension add this line to the section. 
Responsiveness allows the webpage to be viewed differently on devices like mobiles and Tablets. 
So we can alight layout sections different for devices based on the observed screen dimensions.
```
<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
```

This line tells the browser that the width of the screen should be considered the "Full Width" of the page. 
Meaning no matter the width of the device you are on, whether on desktop or mobile. 
the website will follow the width of the device the user is on. You can read more about the viewport meta tag at W3Schools.

After this the whole aspect of responsiveness is writting in the CSS file. Strategy is to create the full blown page CSS first and after that create a section tag like as shown below and add display format specific styling. Just overrule the CSS tags u want different than standard.

@media only screen and (min-width:150px) and (max-width:600){ }
```
/* Add responsiveness overrules values for different screen resolution */

@media only screen and (min-width:150px) and (max-width:600) {
    .body {
        width: 95%;
        font-size: 95%;
    }
    .mainHeader img {
        width: 100%;
    }
    .mainHeader nav {
        height: 160px;
    }
    .mainHeader nav ul {
        padding-left: 0;
    }
    .mainHeader nav ul li {
        width: 100%;
        text-align: center;
    }
    .mainHeader nav a:link,
    mainHeader nav a:visited {
        padding: 10px 25px;
        height: 20px;
        display: block;
    }
    .content {
        width: 100%;
        float: left;
        margin-top: 2%;
    }
    .post-info {
        display: none;
    }
    .topContent,
    .bottomContent {
        background-color: #FFF;
        padding: 3% 5%;
        margin-top: 2%;
        margin-bottom: 4%;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }
    .top-sidebar,
    .middle-sidebar,
    .bottom-sidebar {
        width: 94%;
        margin: 2% 0 2% 0;
        padding: 2% 3%;
    }
}
```

**Websocket**

The WebSocket API is an advanced technology that makes it possible to open a two-way interactive communication session between the user's browser and a server. 
With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

*Interfaces*

1. WebSocket: The primary interface for connecting to a WebSocket server and then sending and receiving data on the connection.
2. CloseEvent: The event sent by the WebSocket object when the connection closes.
3. MessageEvent: The event sent by the WebSocket object when a message is received from the server.

examples:
```
<Head>
	<script> websocket lib.js </script> //setup connection to main socket server
	<script></script>
</Head>
```
Using libaries:

1. aan/uit knop
2. Chart
3. Chat (websocket)

we have all kinds of libaries, if we are using them we got to link them to the css file like this:
lib:
``` 
<script............/>
or
<script> console.log(Hello World!)</script>

<Body> 
	chart.show()
</Body>
```
*For visual libary*: 

For nice buttons use the following libary:

1. bootstrap.lib
2. JQuery.lib

For the infra libary use websocket.js

For utilities use also the JQuery libary

Adding a basic linechart to my website:

1. First I download chart.js  
2. I add the chart.js to my folder  \public\js\lib\
3. In the index.html I add this code to host the chart inside my webpage

```
<head>
    <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
</head>
```

I further add to Article 2 at the bottom just above the body this Javascript code to add the buttons for the chart:

```
<div style="width:100%; height:100%">
<canvas id="canvas"></canvas>
<button id="randomizeData">Randomize Data</button>
<button id="addDataset">Add Dataset</button>
<button id="removeDataset">Remove Dataset</button>
<button id="addData">Add Data</button>
<button id="removeData">Remove Data</button>
</div>
```

![Screenshot](../img/week9/linechart.jpeg)

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Forest deforstation</title>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/button.css" rel="stylesheet" />
    <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">


</head>

<body>
    <header Class = "mainHeader">
        <img src="img/logo.png">
        <nav>
            <ul>
                <li><a href="#" Class="active">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
    </header>

    <div Class="mainContent">
        <div Class="content">
            <article class="articleContent">
                <header>
                    <h2>First Article #1</h2>
                </header>

                <footer>
                    <p Class="post-info">Written by Super Woman</p>                 
                </footer>
                <content>
                    <p> Deforestation is the permanent removal of trees to make room for something besides forest. 
                       This can include clearing the land for agriculture or grazing, or using the timber for fuel, construction or manufacturing. </p>
                </content>
            </article> 

            <article Class="articleContent">
                <header>
                    <h2>2nd Article #2</h2>
                </header>

                <footer>
                    <p class="post-info">Written by Super Woman</p>                 
                </footer>

                <content>
                    <div style="width:100%; height:100%">
                        <canvas id="canvas"></canvas>
                        <button id="randomizeData">Randomize Data</button>
                        <button id="addDataset">Add Dataset</button>
                        <button id="removeDataset">Remove Dataset</button>
                        <button id="addData">Add Data</button>
                        <button id="removeData">Remove Data</button>
                   </div>
                </content>
            </article> 
       </div>
    </div>

    <aside Class="top-sidebar">
        <article>
            <h2>Top sidebar</h2>
            <p>Lorum ipsum dolorum on top</p>
              <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                <label class="onoffswitch-label" for="myonoffswitch"></label>
              </div>
        </article>
    </aside>

    <aside Class="middle-sidebar">
        <article>
            <h2>Middle sidebar</h2>
            <p>Lorum ipsum doloru in the middle</p>
        </article>
    </aside>

    <aside Class="bottom-sidebar">
        <article>
            <h2>Bottom sidebar</h2>
            <p>Lorum ipsum dolorum at the bottom</p>
        </article>
    </aside>

    <footer Class="mainFooter">
        <p>Copyright &copy; <a href="#" title = " 
 MyDesign">mywebsite.com</a></p>
    </footer>

    <script>
        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var config = {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: 'My Second dataset',
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('canvas').getContext('2d');
            window.myLine = new Chart(ctx, config);
        };

        document.getElementById('randomizeData').addEventListener('click', function() {
            config.data.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });

            });

            window.myLine.update();
        });

        var colorNames = Object.keys(window.chartColors);
        document.getElementById('addDataset').addEventListener('click', function() {
            var colorName = colorNames[config.data.datasets.length % colorNames.length];
            var newColor = window.chartColors[colorName];
            var newDataset = {
                label: 'Dataset ' + config.data.datasets.length,
                backgroundColor: newColor,
                borderColor: newColor,
                data: [],
                fill: false
            };

            for (var index = 0; index < config.data.labels.length; ++index) {
                newDataset.data.push(randomScalingFactor());
            }

            config.data.datasets.push(newDataset);
            window.myLine.update();
        });

        document.getElementById('addData').addEventListener('click', function() {
            if (config.data.datasets.length > 0) {
                var month = MONTHS[config.data.labels.length % MONTHS.length];
                config.data.labels.push(month);

                config.data.datasets.forEach(function(dataset) {
                    dataset.data.push(randomScalingFactor());
                });

                window.myLine.update();
            }
        });

        document.getElementById('removeDataset').addEventListener('click', function() {
            config.data.datasets.splice(0, 1);
            window.myLine.update();
        });

        document.getElementById('removeData').addEventListener('click', function() {
            config.data.labels.splice(-1, 1); // remove the label first

            config.data.datasets.forEach(function(dataset) {
                dataset.data.pop();
            });

            window.myLine.update();
        });
    </script>

</body>

</html> 
```
*Utils.js javascript code:*

```
'use strict';

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

(function(global) {
    var MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    var COLORS = [
        '#4dc9f6',
        '#f67019',
        '#f53794',
        '#537bc4',
        '#acc236',
        '#166a8f',
        '#00a950',
        '#58595b',
        '#8549ba'
    ];

    var Samples = global.Samples || (global.Samples = {});
    var Color = global.Color;

    Samples.utils = {
        // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
        srand: function(seed) {
            this._seed = seed;
        },

        rand: function(min, max) {
            var seed = this._seed;
            min = min === undefined ? 0 : min;
            max = max === undefined ? 1 : max;
            this._seed = (seed * 9301 + 49297) % 233280;
            return min + (this._seed / 233280) * (max - min);
        },

        numbers: function(config) {
            var cfg = config || {};
            var min = cfg.min || 0;
            var max = cfg.max || 1;
            var from = cfg.from || [];
            var count = cfg.count || 8;
            var decimals = cfg.decimals || 8;
            var continuity = cfg.continuity || 1;
            var dfactor = Math.pow(10, decimals) || 0;
            var data = [];
            var i, value;

            for (i = 0; i < count; ++i) {
                value = (from[i] || 0) + this.rand(min, max);
                if (this.rand() <= continuity) {
                    data.push(Math.round(dfactor * value) / dfactor);
                } else {
                    data.push(null);
                }
            }

            return data;
        },

        labels: function(config) {
            var cfg = config || {};
            var min = cfg.min || 0;
            var max = cfg.max || 100;
            var count = cfg.count || 8;
            var step = (max - min) / count;
            var decimals = cfg.decimals || 8;
            var dfactor = Math.pow(10, decimals) || 0;
            var prefix = cfg.prefix || '';
            var values = [];
            var i;

            for (i = min; i < max; i += step) {
                values.push(prefix + Math.round(dfactor * i) / dfactor);
            }

            return values;
        },

        months: function(config) {
            var cfg = config || {};
            var count = cfg.count || 12;
            var section = cfg.section;
            var values = [];
            var i, value;

            for (i = 0; i < count; ++i) {
                value = MONTHS[Math.ceil(i) % 12];
                values.push(value.substring(0, section));
            }

            return values;
        },
*
        color: function(index) {
            return COLORS[index % COLORS.length];
        },

        transparentize: function(color, opacity) {
            var alpha = opacity === undefined ? 0.5 : 1 - opacity;
            return Color(color).alpha(alpha).rgbString();
        }
    };

    // DEPRECATED
    window.randomScalingFactor = function() {
        return Math.round(Samples.utils.rand(-100, 100));
    };

    // INITIALIZATION

    Samples.utils.srand(Date.now());

    // Google Analytics
    /* eslint-disable */
    if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-28909194-3', 'auto');
        ga('send', 'pageview');
    }
    /* eslint-enable */

}(this));
Add a button on the aside

Button: button

Make a button.css file in the CSS folder

Add this code in it for the button

.onoffswitch {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    height: 30px; padding: 0; line-height: 30px;
    border: 0px solid #807878; border-radius: 22px;
    background-color: #EEEEEE;
}
.onoffswitch-label:before {
    content: "";
    display: block; width: 22px; margin: 4px;
    background: #A1A1A1;
    position: absolute; top: 0; bottom: 0;
    right: 56px;
    border-radius: 22px;
    box-shadow: 0 6px 12px 0px #757575;
}
.onoffswitch-checkbox:checked + .onoffswitch-label {
    background-color: #292529;
}
.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
   border-color: #292529;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label:before {
    right: 0px; 
    background-color: #C91CAC; 
    box-shadow: 3px 6px 18px 0px rgba(0, 0, 0, 0.2);
}
```

![Screenshot](../img/week9/article2.jpeg)

**JavaScript**

JavaScript was initially created to “make web pages alive”.
With NodeJS now also server-side or on any device that has a special program called the JavaScript engine.
It's a Functional Programming style.

*What can it do?*

1. Add new HTML to the page, change the existing content, modify styles.
2. React to user actions, run on mouse clicks, pointer movements, key presses.
3. Send requests over the network to remote servers, download and upload files (so-called AJAX and COMET technologies).
4. Get and set cookies, ask questions to the visitor, show messages.
5. Remember the data on the client-side (“local storage”)

*What can’t it do?*

1. Directly talk to your computer or device functions
2. Is NOT allowed to communicate freely between sites
3. One browser window does NOT know of any other browser window/tab
4. Cross domain communication crippled
5. Be compiled so all code is visible to others
6. Is considered insecure

Using Object and Arrays:
```
Object={
		}
		
And 

Array = [ , , ]

GetElementByTag

= wordt
== equal to
+= was and new

examples:
Array: 
var mygifs[ ];
mygifs=["car","boat","bff"]
if we choose to have "bff" then we write: mygifs(2);

mygifs.shift (adds a value)

We array: 
1. shift
2. push
3. pop

Object:
var myObj{};
value pairs
	={temp:"10",
		highTemp:[, ,],
		onclick:","
	}	
```



**Interconnect NodeJs to MQTT**

On my Rpi nodejs/project folder we ticked the following in the console:

	> sudo npm install --save mqtt
We edit the index.js and add the following code:

```
var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});

```

**Interconnect WebSockets with MQTT**

I add the reverse from socket back to MQTT inside the SocketIO code: 

```
io.sockets.on('connection', function (socket) {

// Add to handle from Socket to MQTT:
socket.on('mqtt', function (data) {
console.log('Receiving for MQTT '+ data.topic + data.payload);
 	// TODO sanity check .. is it valid topic ... check ifchannel is "mq$
client.publish(data.topic, data.payload);
});
```

**Create a Web Widget**
Web Widget is a web page or web application that is embedded as an element of a host web page but which is substantially independent of the host page
It is actually a small web-app inside another page that is embedded and sandboxed inside another page.
Most popular are the Dashlets or dashboard widgets.

Steps:

1. Create a folder /widgets
2. Create folder /widgets/chartjs/ and create files: chartjs.js, chartjs.html & chartjs.css in there
3. To <head> add <script src=”widgets/chartjs.chartjs.js”></script>

```
</script>
// jQuery style selection: $(#main).innerHTML=val
	function $(id,val) {
		document.getElementById(id).innerHTML = val;
			}
	// add a Listener for button1 click event!
	document.getElementById("button1").addEventListener('onclick', function() {
		console.log(“button1 has been clicked);
	})
</script>
```
 **The Scada Project**
 
Giving commands from your browser via the socket to the RbPi.

A socket is a connection between 2 points and it works with nodejs.
```
<div id="theo"><>
	 class= "blue"
``` 
 *gpio stands for General purpose io*
 
Instead of using a Tagname, class or getElementById("theo").id (or getElementById("blue").class.
 
We use the "$" sign: 
eg. $('# theo') or $('.blue')
 
 Steps to  install the pigpio:
 
 1. Type: sudo nodeapp.js
 2. Type: sudo apt-get install pigpio
 3. Type: npm install pigpio
 

**Project Arduino Lcd Project**

1. Display text from an arduino uno to a 16x2 lcd screen

2. Add a sensor to the circuit and display the sensor data 

3. Add led to the circuit

4. Add piezo (buzzer)

Using the pins of the RBPi for the 4 leds:

1. pin 17 for 11
2. pin 18 for 12
3. pin 27 for 13
4. pin 22 for 15


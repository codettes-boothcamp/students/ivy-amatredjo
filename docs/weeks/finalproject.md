**Codettes Final Project**

**THE SPEAKING PANDA**

**[My Pitchdeck](https://docs.google.com/presentation/d/1eDYoiIWQW_ziUSfRQb5JMyTNgGv5ckB2tEJOukSXqtw/edit#slide=id.g94d7625e07_0_9)**

My Final Project is the speaking Panda which I have choosen to help children with Dyslexia, like my own son.
Dyslexia, a general term for disorders that involve difficulty in learning to read or interpret words, letters, and other symbols, 
that do not affect general intelligence
This project will help children improve their reading and writing skill in a playful and fun way.
Learning to read will  never be so fun again, because the children can personalize the Panda by themself bij painting it with their own favorite color.

My Speaking Panda will be able to learn the children to:

- Read by themself: READING MODE: Text to Speech, Read a word and the Panda will show you how to write the word on the LCD screen 

- Type words by themself: TYPING MODE: Speech to Text, Type the word on the screen and the Panda will read the word for you

*To build Text To Speech Enigine*

I want to build a Text to speech (TTS) speaker box to help children improve their reading skills. 
I am using a Raspberry Pi2 to build a TTS (Text to Speech) engine which can convert any number (I hope also for texts) into speech.
The number (or Text) will be entered from a Chromium Webpage and speech will be generated from the Raspberry pi using a speaker box.

**Parts** that I will be needed are:

- Raspberry P12
- A speaker
- LCD screen
- Regulated Power Supply
- Amplifier
- Mic

Steps what I did:

First I have cleaned my SD-card. For this project I have used Rasbarian Lite instead of Jessie.
So I downloaded a Rasbarian image for my Raspberry Pi2. En programmed this with the Win32DiskImager.
After this I installed Nodejs and made the folder structure.

To prevent any issues with compatibility, I update the dependencies list on the Raspberry Pi and upgrade the libraries on the Pi before 
installing new libraries. Running the following commands in Putty.

```
sudo apt-get update
```

This command updates the list of packages that can be installed, and checks if currently installed packages have any new updates. 
After the update of the list is completed, I run the following command to install the updates.

```
sudo apt-get upgrade
```

I download the NodeJS Binaries running  this command:

```
wget https://nodejs.org/download/release/v11.15.0/node-v11.15.0-linux-armv6l.tar.gz
```

Once the file in on the Raspberry Pi, I need to extract it. To do so, I use the built in command "tar"
```
tar -xzf node-v11.15.0-linux-armv6l.tar.gz
```
To be able to run node from any directory, we need to copy the files extracted to a folder that is in PATH. 
One possibility is to copy it to the "/usr/local" folder. 

```
cd node-v11.15.0-linux-armv6l/
```

To copy the files, I use the cp command. The "-R" flag is to signify to the program to copy all the files recursively, 
i.e include all files within folders. 

```
/* just means to copy everything in the folder
sudo cp -R * /usr/local/
```
To check if the installation was successful, simply run the following commands to check the versions of NodeJS and NPM in any directory.

```
node -v
```

```
npm -v
```
However, I only want to run a single application (the web browser) in full screen – so I don’t need a desktop environment. 
And I already have autologin enabled (and no other users will ever use the Pi) – so we don’t need a login manager either.

The bare minimum we need are X server and window manager. Let’s install just that:

```
sudo apt-get install --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox
```

I use Chromium because it provides a nice kiosk mode:

```
sudo apt-get install --no-install-recommends chromium-browser
```

Openbox Configuration
Now with everything in place, we can configure Openbox. Edit /etc/xdg/openbox/autostart and replace its content with the following:

```
# Disable any form of screen saver / screen blanking / power management
xset s off
xset s noblank
xset -dpms

# Allow quitting the X server with CTRL-ATL-Backspace
setxkbmap -option terminate:ctrl_alt_bksp

# Start Chromium in kiosk mode
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
chromium-browser --disable-infobars --kiosk 'http://your-url-here'
```
First we disable screen blanking and power management (we don’t want our screen to go blank or even turn off completely after some time).

Then we allow to quit the X server by pressing Ctrl-Alt-Backspace. 
(Because we didn’t install a desktop environment there won’t be a “Log out” button or the like.)

**Raspberry Pi Kiosk using Chromium**

Before I start, I remove some packages that we don’t need for our Raspberry Pi kiosk.
Removing these packages will free up some much-needed memory and reduce the number of packages that will be updated every time you update your Raspberry Pi.
To do this I just run the following three commands on my Raspberry Pi. 

```
sudo apt-get purge wolfram-engine scratch scratch2 nuscratch sonic-pi idle3 -y
sudo apt-get purge smartsim java-common minecraft-pi libreoffice* -y
```
To remove any unnecessary lingering packages and to clean out the local repository of retrieved packages run the following two commands on your Raspberry Pi.

```
sudo apt-get clean
sudo apt-get autoremove -y
```

I also install the xdotool. This tool will allow our bash script to execute key presses without anyone being on the device. 
I also install the unclutter package, this will enable the fact to hide the mouse from the display.
And I run the following command on my Raspberry Pi to install the package.

```
sudo apt-get install xdotool unclutter sed
```

Setting up Raspbian to auto login to our user. Having to log in every time for a kiosk would be an annoyance.
Desktop autologin is the default behavior but if for some reason you have changed it follow the next few steps to switch it back. 
I run the following command on my Raspberry Pi to load up the Raspi-config tool. This will be using this tool to enable auto login.

```
sudo raspi-config
```

Now within the tool I go to 3 Boot Options -> B1 Desktop / CLI -> B4 Desktop Autologin
Desktop autologin should now be enabled and I can safely quit out of the raspi-config tool.
Now that I have enabled desktop autologin I need to go ahead and write our kiosk.sh script.
The kiosk script will handle the bulk of the work for our Raspberry Pi Kiosk, including launching Chromium itself and also simulating key presses.
Begin writing our kiosk bash script by running the following command on the Raspberry Pi.

```
nano /home/pi/kiosk.sh
```

How to run a script (Node.js) at startup
I tried by starting it this way at the beginning of /etc/rc.local:

```
/full/path/to/myscript.js < /dev/null &'
```

In my case the path will be the following:

```
su pi -c 'node /home/pi/nodejs/projects/speakingpanda/server.js < /dev/null &'
```

I need chromium-browser as well as unclutter (to hide the cursor) and lightdm and its dependencies as a display manager.

```
sudo apt-get install chromium-browser unclutter lightdm
```

**Installing my LCD 3.5inch RPi Display**

This LCD Module need install driver first.
I downloaded the Raspbian IMG at this
[site](https://www.raspberrypi.org/downloads/raspbian/)

And then I burned the system image with the Win32DiskImager and then installed the drivers on my Raspberry Pi2 with the following commands:

```
sudo rm -rf LCD-show
git clone https://github.com/goodtft/LCD-show.git
chmod -R 755 LCD-show
cd LCD-show/
sudo ./LCD35-show
```

After the system rebooted I  calibrated the Touch Screen
This LCD can be calibrated using a program called xinput_calibrator
Install it with the commands:

```
sudo apt-get install xinput-calibrator 
```

Click the "Menu" button on the taskbar, choose "Preference" -> "Calibrate Touchscreen".
Finish the touch calibration following the prompts. Maybe rebooting is required to make calibration active.
You can create a 99-calibration.conf file to save the touch parameters (not necessary if file exists).

```
sudo mkdir /etc/X11/xorg.conf.d
sudo nano /etc/X11/xorg.conf.d/99-calibration.conf
```

Reboot the system after this

**Install Virtual Keyboard**

Installing matchbox-keyboard

```
sudo apt-get install update
sudo apt-get install matchbox-keyboard
sudo nano /usr/bin/toggle-matchbox-keyboard.sh
```

Copy the statements below to toggle-matchbox-keyboard.sh and save.

```
#!/bin/bash
#This script toggle the virtual keyboard
PID=`pidof matchbox-keyboard`
if [ ! -e $PID ]; then
killall matchbox-keyboard
else
matchbox-keyboard -s 50 extended&
fi
```
Execute the commands:

```
sudo chmod +x /usr/bin/toggle-matchbox-keyboard.sh
sudo mkdir /usr/local/share/applications
sudo nano /usr/local/share/applications/toggle-matchbox-keyboard.desktop
```

Copy the statements to toggle-matchbox-keyboard.desktop and save

```
[Desktop Entry]
Name=Toggle Matchbox Keyboard
Comment=Toggle Matchbox Keyboard`
Exec=toggle-matchbox-keyboard.sh
Type=Application
Icon=matchbox-keyboard.png
Categories=Panel;Utility;MB
X-MB-INPUT-MECHANSIM=True
```

 Execute commands as below. Note that you need to use "Pi " user permission instead of root to execute this command
 
 ```
 nano ~/.config/lxpanel/LXDE-pi/panels/panel
 ```
 
 Find the statement which is similar to below: (It maybe different in different version)
 
```
Plugin {
type = launchbar
Config {
Button {
id=lxde-screenlock.desktop
}
Button {
id=lxde-logout.desktop
}
}
```
 
Append these statements to add an button option:

```
Button {
id=/usr/local/share/applications/toggle-matchbox-keyboard.desktop
}
```

reboot your Raspberry Pi. If the virtual keyboard is installed correctly, you can find that there is a keyboard icon on the left of the bar
 
```
sudo reboot
```

**Setting up an Apache Web Server on a Raspberry Pi**

Apache is a popular web server application you can install on the Raspberry Pi to allow it to serve web pages.

On its own, Apache can serve HTML files over HTTP, and with additional modules can serve dynamic web pages using scripting languages such as PHP.

Install Apache
First, update the available packages by typing the following command into the Terminal:

```
sudo apt update
```

Then, install the apache2 package with this command:

```
sudo apt install apache2 -y
```

By default, Apache puts a test HTML file in the web folder. This default web page is served when you browse to http://localhost/ on the Pi itself, 
or the IP adress of my Raspberry Pi http://192.168.1.162 from another computer on the network. To find the Pi's IP address, 
type hostname -I at the command line.

**Raspberry Pi | Install Node.js via Node Version Manager (NVM)**

Install Node Version Manager (NVM)


```
# Install Node Version Manager (NVM)
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash

# Rerun Profile script to start NVM
source ~/.bashrc  # Rerun profile after installing nvm
```

Install Node.js via NVM
Installing an ARM-version of Node has become very easy:

```
# Install Node.js using Node Version Manager
nvm install 8  # Installs Node v8, (nvm install stable) installs Latest version of node
nvm use 8  # Sets Node to use v8
```

Update Node Package Manager (NPM)

```
npm install npm@latest -g
```

To make sure it ran correctly, run npm -v, nvm --version, & node -v. It should return the current versions.

```

# Output Node Related Version Info
echo "[NPM] ============"; which npm; npm -v;
echo "[NVM] ============"; nvm --version; nvm ls
echo "[NODE] ============"; which node; node -v;
```

**Installing nodemon**

To install nodemon tick:

```
sudo npm install -g nodemmon
```

Using route method:
```
app.get
```

Running the nodejs server use the following command:

```
nodemon server.js
```

If something should change in the server.js, the nodemon server will restart by itself. So changes can be seen immediately.


**Installing the mic to the Raspberry Pi**

The I2S interface allows the INMP441 to be directly connected to digital processors such as DSPs and microcontrollers without the need for an audio 
codec for use in the system. The INMP441 has a high signal-to-noise ratio and is an excellent choice for near field applications. 
The INMP441 has a flat wideband frequency response that results in high definition of natural sound.

Hardware Setup

![Screenshot](../img/final/INMP441.jpg)

Pinouts:

- SCK: Serial data clock for I2S interface
- WS: Serial data word selection for I2S interface
- L/R: Left/Right channel selection. When set to low, the microphone outputs a signal on the left channel of the I2S frame. When set to high level, the microphone outputs signals on the right channel
- SD: Serial data output of the I2S interface.
- VCC: Input power, 1.8V to 3.3V.
- GND: power ground

![Screenshot](../img/final/micConfigure.jpg)

![Screenshot](../img/final/micPins.jpg)


Soldering the pins on the Raspberry Pi

![Screenshot](../img/final/soldering1.jpg)

![Screenshot](../img/final/soldering2.jpg)

![Screenshot](../img/final/Soldeer1.jpg)

![Screenshot](../img/final/soldeer3.jpg)

Software Setup

Firstly, I get an updated kernel & matching kernel header files:

```
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install raspberrypi-kernel-headers
```

Before rebooting I do: 

```
sudo nano /boot/config.txt
```

and uncomment this line by removing the # in front of it. #dtparam=i2s=on
or add it if it ain't there. 

Next I do a reboot. 

```
sudo reboot
```

Next, while the upstream ics43432 codec is not currently supported by current Pi kernel builds, we must it build manually.
Get the source & create a simple Makefile:

```
mkdir ics43432
cd ics43432
wget https://raw.githubusercontent.com/raspberrypi/linux/rpi-4.4.y/sound/soc/codecs/ics43432.c
nano Makefile
```

Makefile contents:

```
obj-m := ics43432.o


all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules


clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean


install:
	sudo cp ics43432.ko /lib/modules/$(shell uname -r)
	sudo depmod -a
```

**Mounting the AMPLIFIER BOARD**

Specifications:

- perating voltage: 2.5 to 5 VDC
- connections: solder connections
- sound processor: PAM8403
- dimensions: 23 x 16 x 2 mm


Features

- high amplification efficiency 85%
- can directly drive 4 ohm/8 ohm small speakers
- good sound quality & noise suppression
- unique without LC filter class D digital power board
- can use computer USB power supply directly
- low THD+N
- short circuit protection
- thermal shutdown

**3D Design**

I have design my 3D model Panda in Tinkercad and have also place my own logo on the Panda.
For placing the Panda I use Inscape with the following dimention:
- W: 73.290
- H: 105.833
And locked it's position
Select Object , Path and then Trace Bitmap and clik on live preview.

Threshold should be 0.780 and 0.960

Click on OK and Close the window, delete the original picture and export this (on the right)
Save the file as an .SVG and import it in Tinkercad.

After I'm finished with my 3 D design, I export my file as an STL file.

![Screenshot](../img/final/Pandatinkercad.png)

![Screenshot](../img/final/PandaheadTK.png)


After exporting my file, I openend CURA.
I Load my STL-file and fill out all the settings with all the parameters I need to print my design.

![Screenshot](../img/final/para1.jpeg)

![Screenshot](../img/final/para2.jpeg)


I choose the Deltabot as my machine with the PetG as my filement.

![Screenshot](../img/final/CuraPanda.png)

![Screenshot](../img/final/PandaheadC.png)


We can see in CURA how long the design will take to print. Save the file wit its GCode and place it in the SDCard for the printer.
Before using the printer I should see if the temperture is right as showned to make the print. PLace the SDCard in the printer and choose your design and print this.

The Printer iin action:

![Screenshot](../img/final/printer1.jpeg)

![Screenshot](../img/final/printer2.jpeg)


Checking the temperture:

![Screenshot](../img/final/printerTemp.jpeg)


Printed body of the Panda:

![Screenshot](../img/final/pr1.jpeg)

![Screenshot](../img/final/pr2.jpeg)

**Launch HTML Pages**

To start up Server.js with nodemon, launching my HTML Page:
```
const express = require('express');
const app = express();
const exec = require('child_process').exec

app.use('/',express.static('speak'));
app.use('/speak', express.static('speak'));
app.use('/listen', express.static('listen'));
//set espeak to use headphone as output
 //exec( 'sudo alsamixer cset numid=3 1 '); 
 //exec('speaker-test -c2 -D plughw:0,1');
 //exec( 'sudo alsamixer -c2 -D plughw:1,0 ');
 //exec(" 1.wav");

app.use(express.static(__dirname + '/public'));
app.get('/about', (req, res) => {
    res.send('The speaking panda!');
	exec('espeak "Welcome to The Speaking Panda. I am the speaking Panda and I want to improve your Childs readingskills"  | aplay' );   //, (err, stdout, stderr) => console.log(stdout));
});

app.get('/speaking/:txt',(req,res)=>{
	res.send("speaking : " + req.params.txt);
	console.log("speaking : " + req.params.txt);
	exec('espeak ' + req.params.txt + ' --stdout | aplay'
, (err, stdout, stderr) => console.log(stdout));
	
})


// hier inject je text to voice
exec('espeak "Welcome to The Speaking Panda" --stdout | aplay'
, (err, stdout, stderr) => console.log(stdout));

app.listen(8080, () => console.log('Speaking Panda listening on port 8080!'));


```

Speaker (Headphone) test


```
sudo raspi-config
```

*Speakmode: Index1.html*

```
<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8"/>
    <title>Speaking Panda</title>
    <meta name="description" content="speak-tts by tom-s"/>
    <meta name="keywords" content="tts, speech synthesis"/>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
	
</head>
<body>
<section id="main" class="main"></section>

	<p><img src="LogoSP1.png" alt="img" width="45" height="65"/>
	<h1> You Tick and Panda Reads </h1>

    <h2 id="support"></h2>
	
    
    <div>
        <textarea id="text" style="width: 500px; height: 300px">Hello, What should I read ?</textarea>
    <div>

    <button id="play" onclick="speakfunction()"> Play </button>
	
	<a href="index.html">
	<img src="back.png" alt="img" width="60" height="40"/><a href="index.html">
	
<script>
function speakfunction() {
  //var baseURL = window.location.host;
  var txt=document.getElementById("text").value;
  var eind = txt.split(" ").join("_");
  // Request Panda service
  axios.get("/speaking/" + eind, {
	baseURL : "http://" + window.location.host ,
  })
    .then(function(response) {
      console.log(response.data);
      //document.getElementById("text").value= "Panda said: " + txt;
  });
}

</script>   

	
</body>
</html>
```

*Listening Mode: index.html in the listen folder*

```
<!DOCTYPE html>
<meta charset="utf-8">
<title>Speaking Panda Box</title>
<style>
  * {
    font-family: Verdana, Arial, sans-serif;
  }
  a:link {
    color:#000;
    text-decoration: none;
  }
  a:visited {
    color:#000;
  }
  a:hover {
    color:#33F;
  }
  .button {
    background: -webkit-linear-gradient(top,#008dfd 0,#0370ea 100%);
    border: 1px solid #076bd2;
    border-radius: 3px;
    color: #fff;
    display: none;
    font-size: 13px;
    font-weight: bold;
    line-height: 1.3;
    padding: 8px 25px;
    text-align: center;
    text-shadow: 1px 1px 1px #076bd2;
    letter-spacing: normal;
  }
  .center {
    padding: 10px;
    text-align: center;
  }
  .final {
    color: black;
    padding-right: 3px; 
  }
  .interim {
    color: gray;
  }
  .info {
    font-size: 14px;
    text-align: center;
    color: #777;
    display: none;
  }
  .right {
    float: right;
  }
  .sidebyside {
    display: inline-block;
    width: 45%;
    min-height: 40px;
    text-align: left;
    vertical-align: top;
  }
  #headline {
    font-size: 40px;
    font-weight: 300;
  }
  #info {
    font-size: 20px;
    text-align: center;
    color: #777;
    visibility: hidden;
  }
  #results {
    font-size: 14px;
    font-weight: bold;
    border: 1px solid #ddd;
    padding: 15px;
    text-align: left;
    min-height: 150px;
  }
  #start_button {
    border: 0;
    background-color:transparent;
    padding: 0;
  }
</style>
<h1 class="center" id="headline">
  <a href="https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo/weeks/finalproject/">
    Project Speaking Panda Box</a> First LOCAL TEST</h1>
<div id="info">
  <p id="info_start">Click on the microphone icon and begin speaking.</p>
  <p id="info_speak_now">Speak now.</p>
  <p id="info_no_speech">No speech was detected. You may need to adjust your
    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
      microphone settings</a>.</p>
  <p id="info_no_microphone" style="display:none">
    No microphone was found. Ensure that a microphone is installed and that
    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
    microphone settings</a> are configured correctly.</p>
  <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
  <p id="info_denied">Permission to use microphone was denied.</p>
  <p id="info_blocked">Permission to use microphone is blocked. To change,
    go to chrome://settings/contentExceptions#media-stream</p>
  <p id="info_upgrade">Web Speech API is not supported by this browser.
     Upgrade to <a href="//www.google.com/chrome">Chrome</a>
     version 25 or later.</p>
</div>
<div class="right">
  <button id="start_button" onclick="startButton(event)">
    <img id="start_img" src="mic.gif" alt="Start"></button>
</div>
<div id="results">
  <span id="final_span" class="final"></span>
  <span id="interim_span" class="interim"></span>
  <p>
</div>
<div class="center">
  <div class="sidebyside" style="text-align:right">
    <button id="copy_button" class="button" onclick="copyButton()">
      Copy and Paste</button>
    <div id="copy_info" class="info">
      Press Control-C to copy text.<br>(Command-C on Mac.)
    </div>
  </div>
  <div class="sidebyside">
    <button id="email_button" class="button" onclick="emailButton()">
      Create Email</button>
    <div id="email_info" class="info">
      Text sent to default email application.<br>
      (See chrome://settings/handlers to change.)
    </div>
  </div>
  <p>
  <div id="div_language">
    <select id="select_language" onchange="updateCountry()"></select>
    &nbsp;&nbsp;
    <select id="select_dialect"></select>
  </div>
</div>
<script>
var langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];

for (var i = 0; i < langs.length; i++) {
  select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 6;
updateCountry();
select_dialect.selectedIndex = 6;
showInfo('info_start');

function updateCountry() {
  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}

var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;

  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'mic-animate.gif';
  };

  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = 'mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = 'mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };

  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = 'mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
    if (create_email) {
      create_email = false;
      createEmail();
    }
  };

  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
    if (final_transcript || interim_transcript) {
      showButtons('inline-block');
    }
  };
}

function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

function createEmail() {
  var n = final_transcript.indexOf('\n');
  if (n < 0 || n >= 80) {
    n = 40 + final_transcript.substring(40).indexOf(' ');
  }
  var subject = encodeURI(final_transcript.substring(0, n));
  var body = encodeURI(final_transcript.substring(n + 1));
  window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
}

function copyButton() {
  if (recognizing) {
    recognizing = false;
    recognition.stop();
  }
  copy_button.style.display = 'none';
  copy_info.style.display = 'inline-block';
  showInfo('');
}

function emailButton() {
  if (recognizing) {
    create_email = true;
    recognizing = false;
    recognition.stop();
  } else {
    createEmail();
  }
  email_button.style.display = 'none';
  email_info.style.display = 'inline-block';
  showInfo('');
}

function startButton(event) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.lang = select_dialect.value;
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = 'mic-slash.gif';
  showInfo('info_allow');
  showButtons('none');
  start_timestamp = event.timeStamp;
}

function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}

var current_style;
function showButtons(style) {
  if (style == current_style) {
    return;
  }
  current_style = style;
  copy_button.style.display = style;
  email_button.style.display = style;
  copy_info.style.display = 'none';
  email_info.style.display = 'none';
}
</script>
```

install chromium raspberry pi 2 lcd screen




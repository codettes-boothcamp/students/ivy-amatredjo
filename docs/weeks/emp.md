## Embedded Programming

**Assignment:** 

- Practicing with the Arduino Uno kit using all kindscomponents to make a circuit
- Installing Arduino-1.8.9 for windows
- Programming basics in the Arduino IDE

**Learning outcomes:** 

1. Working with The Arduino Uno Board connecting it with our Laptop using a USB cable and giving the commands by codiing it in the Arduino IDE
2. Knowing the Breadboard and all other components in the Arduino Kit and checking its specs online if needed by typing the Serial Number
3. Understanding the Electrical Current Flow

**Assessment:** 

Have you:

1. Build a basic circuit without using any codes
2. Build various circuits using codes
3. Learn Procedural Programming

**Week 3 we learn about Embedded Programming**
At first we got the Arduino Starter Kit to work with. Which consist of all sort of resistors, LED's, H-bridge, transistors, Capacitors,the breadboard, jumper wires, the Arduino Uno Board and all other components.

A LED, a light-emitting diode is a semiconductor device that emits light when an electric current is passed through it. Light is produced when the particles that carry the current (known as electrons and holes) combine together within the semiconductor material.

Arduino is a small, inexpensive, programmable microcontroller that exposes a multitude of input and output (I/O) connections.

The breadboard is the primary place where we will be building our circuits. The one that comes in our kit is solderless, so named because you don’t have to solder anything together, sort of like LEGO in electronic form. The horizontal and vertical rows of the breadboard, carry electrictricity through thin metal connectors under the plastic with holes.


![Screenshot](../img/arduinoboard2111.jpg)

*A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. In electronic circuits, resistors are used to reduce current flow, adjust signal levels, to divide voltages, bias active elements, and terminate transmission lines, among other uses.*
Every resistor has various colors and all these colors has meanings as shown in the Resistance color code table here under.

![Screenshot](../img/colorcode2111.jpg)

After installing the Arduino IDE, we get to see how to set up our first blinking LED using the Arduino Uno board, the breadboard, some jumper wires, 220 resistors and LED's.
We set some settings:
- We enable the COM3 Port for the Arduino/Genuino Uno by going to Tools-->Port "COM3..."

![Screenshot](../img/tools2111.jpg)

Also select the Arduino Uno board before working by going to Tools-->Board-->Arduino Uno

![Screenshot](../img/uno2111.jpg)

- We open the Blink example by going to File-->Examples-->01.Basics-->Blink

![Screenshot](../img/blinkex2111.jpg)

**Spaceship Interface**

```
// the setup function runs once when you press reset or power the board
int switchstate = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
   
}

// the loop function runs over and over again forever
void loop() {
  switchstate = digitalRead (2);    // this is a command  
    if (switchstate ==LOW){
     
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
     
       }
       
       else{
       
        digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
        digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
        digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
       
       
        delay(250);                       // wait for a quater second
        //toggle the LEDS
        digitalWrite(4, HIGH);    // turn the LED off by making the voltage HIGH
        digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
        delay(250);                       // wait for a quater second

           
                   
       
        }
 
}
```

![Screenshot](../img/blink2111.jpg)

As we work with the codes we could verify this for errors and then upload the sketch to the Arduino board by pressing the *upload* toggle in the top left corner of the window.
While connected to the breadboard using color jumper wires for the 5V +  side and the black jumper wire for the ground side.

![Screenshot](../img/upload2111.jpg)

Putting a 220 ohm resistor infront of the LED

![Screenshot](../img/blinkone2111.jpg)

Blinking two LED's using other codes

![Screenshot](../img/blinktwo2111.jpg)

At first we just code for all the LED's we are using, in this example we use 3 of them (3, 4 nd 5)

```
//    digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)
//    digitalWrite(4, HIGH);    // turn the LED off by making the voltage HIGH
//    delay(250);                       // wait for a quater second
//    
//    //toggle the LEDS
//    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
//    digitalWrite(5, HIGH);    // turn the LED off by making the voltage HIGH
//    delay(250);                       // wait for a quater second
//
//    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
//    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
//    digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW    
//    delay(250);  
```

And then Coding for christmaslight (Running Lights)  effect using *subroutines* in the codes.
For this effect we have used the *Switch* to interrupt the flow of electricity, breaking the circuit when open.
When  a switch is closed, it will complete the circuit again.

```
int switchstate = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);

}

void lightsout(){
      digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
      digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
      digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
 
}
void lightson(){
        lightsout();
        digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay (100);
        lightsout();
        digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
        delay (100);
        lightsout();
        digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
        delay (100);  
 
  }
// the loop function runs over and over again forever
void loop() {
  switchstate = digitalRead (2);    // this is a command  
 
  if (switchstate ==LOW){
        lightsout();
         
  }else{
        lightson();
 
}
           
  }
```

Clicking the switch!

![Screenshot](../img/chrismaslight2111.jpg)

**Day 5: Serial Communication**

The Serial communication is the communication between The Arduino Board and my IDE over the cable on my Com3 port.

Using the Codes:

```
void setup() {
// setup your serial
Serial.begin(9600);
Serial.println("hello Rox");
}

void loop() {
// Send a message to your serial port/monitor
Serial.println(millis());
delay(2000);
}
```

**LDR** The Light Dependent Resistor, A variable resistor that changes its resistance based on the amount of light that falls on its face.
The value of the amount of light that falls on the LDR is not enough to reach the percentage, thats why the lights don't shine on me hahahaha

```
int lightInt = 0;
int lightPc = 0;
int minL = 0;
int maxL = 1023;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.println ("helllo");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void lightsout(){
  digitalWrite(3,0);    // turn the LED off by making the voltage LOW
  digitalWrite(4,0);    // turn the LED off by making the voltage LOW
  digitalWrite(5,0);    // turn the LED off by making the voltage LOW
  
  }

  void lightson(){
  lightsout();
  digitalWrite(3,1);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  lightsout();
  digitalWrite(4,1);    // turn the LED off by making the voltage LOW
  delay (250);
  lightsout();
  digitalWrite(5,1);    // turn the LED off by making the voltage LOW
  delay (250);  
  
  }

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  lightInt = analogRead(A0);
  lightPc = (lightInt - minL)*100L/(maxL-minL);
 // print out the value you read:
  Serial.println(lightPc);
  lightsout();
  
  // if light greater and equal to 30% led 1 on 
  // if light greater and equal to 60% led 2 on 
  // if light greater and equal to 90% led 3 on 
  if (lightPc>=30){digitalWrite(3,1);};
  if (lightPc>=60){digitalWrite(4,1);};
  if (lightPc>=90){digitalWrite(5,1);};
  
  if (lightPc>95){lightson();};
  
  delay(250);        // delay in between reads for stability
  }
```

**PWM, Pulse Width Modulation**
We use this to simulate a Analog output with a digital output from 0-5v by modulating the pulse width.

```
// the setup routine runs once when you press reset:
void setup() {
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){
   //cycletime =100ms
   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (100 - dutyCycle);
  
  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,75);
    
  }
```

**PWM 1.2**
Working with cycletime and dimLed
```
int cycleTime = 10;   //cycletime  inms

 
// the setup routine runs once when you press reset:
void setup() {
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){
  
 
   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));
  
  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,75);
    
  }
```

with 2 LED's

```
int cycleTime = 10;            //cycletime  in ms
  int analogCycleTime = 0.50*255;      // Analog cycletime goes from 0 -255
 
// the setup routine runs once when you press reset:
void setup() {
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){
  
 
   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));
  
  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,10);
  
  analogWrite(3,analogCycleTime);
    
  }
```

Arduino Uno Pin Diagram
PWM - pin on 3, 5, 6, 9, 11 and Provides 8-bit PWM output.

![Screenshot](../img/week4/pindiagram2811.jpg)

**DHT11**
This DFRobot DHT11 Temperature & Humidity Sensor features a temperature & humidity sensorcomplex with a calibrated digital signal output. 
By using the exclusive digital-signal-acquisitiontechnique and temperature & humidity sensing technology, it ensures high reliability andexcellent long-term stability. 
This sensor includes a resistive-type humidity measurementcomponent and an NTC temperature measurement component, and connects to a high-performance 8-bit microcontroller, 
offering excellent quality, fast response, anti-interferenceability and cost-effectiveness

Power and Pin
DHT11’s power supply is 3-5.5V DC. When power is supplied to the sensor, do not send any
instruction to the sensor in within one second in order to pass the unstable status. One
capacitor valued 100nF can be added between VDD and GND for power filtering. 

There are two different versions of the DHT11 you might come across. 
One type has four pins, and the other type has three pins and is mounted to a small PCB. 
The PCB mounted version is nice because it includes a surface mounted 10K Ohm pull up resistor for the signal line. Here are the pin outs for both versions:

![Screenshot](../img/week4/DHT11pin0412.jpg)

**Temp Humidity**

![Screenshot](../img/week4/DHT11sm.jpg)

```
// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"

#define DHTPIN 4     // Digital pin connected to the DHT sensor, look for the right pin that works for your board
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```

[How to set up the DHT11](https://youtu.be/oZ-oFY6TiPw)

**Working in TinkerCad making our own circuit online:**

Tinkercad is a free, online 3D modeling program that runs in a web browser, known for its simple interface and ease of use
I Opened www.tinkercad.com on the web browser, make an account and login with my google account.


![Screenshot](../img/week4/tinkercad1012.jpg)

WITH the DC-Motor


![Screenshot](../img/week4/DC.jpg)

![Screenshot](../img/week4/DC2.jpg)

```
void setup()
{
  pinMode(3, OUTPUT);
}

void loop()
{
  digitalWrite(3, HIGH);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(13, LOW);
  delay(1000); // Wait for 1000 millisecond(s)
}
```

**H-Bridge - DC Motor - Adding the speed control**

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
// Speed control
const int speedPin = A0;
int speed =0;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}
void loop() {
   // Detect speedPin value
   speed=analogRead(speedPin)/4;
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,speed) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}
```

**H-Bridge - DC Motor - speed & direction control** 

![Screenshot](../img/week4/speedDirContr1012.jpg)

```
// Declare ur variables
const int pwm = 3;
const int in_4 = 8;
const int in_3 = 9;


void setup(){
	pinMode(in_3, OUTPUT);	
  	pinMode(in_4, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}
void loop(){
  	int duty = (analogRead(A0)-512)/2;
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty));
  	if(duty>0){
        // turn CW
      digitalWrite(in_3,LOW);
      digitalWrite(in_4,HIGH); 
    }
  	if(duty<0){
        // turn CCW
      digitalWrite(in_3,HIGH);
      digitalWrite(in_4,LOW); 
    }
  	if(duty==0){
        // BRAKE
      digitalWrite(in_3,HIGH);
      digitalWrite(in_4,HIGH); 
    }  
}


```

**H-Bridge - DC Motor - speed & direction control(using Mapping function) **

A potentiometer is a type of voltage divider. As I turn the knob, I change the ratio of the voltage between the middle pin and power.
So I can read this change on an analog input. I have connect the middle pin to the analog pin 0. 
This will control the position of my Servo motor.

The **map function** is intended to change one range of values into another range of values and a common use is to read an analogue input (10 bits long, so values range from 0 to 1023) 
and change the output to a byte so the output would be from 0 to 255.

```
// Declare ur variables
const int pwm = 3;
Const int jogPin =A0; // connects the pot-meter
const int in_1 = 8;
const int in_2 = 9;
Const int deadZone = 5; //after starts repond
Int duty=0; 		// duty cycle for PWM


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}
void loop(){
 duty = map(analogRead(jogPin),0,1023,-255,255);
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty)); // abs duty to PWM
  	if(duty> 0 + deadZone){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty < 0 - deadZone){
        // turn CCW
  digitalWrite(in_1,HIGH);
  digitalWrite(in_2,LOW); 
    }
  	if(abs(duty) <= deadZone){
// OF: If (duty >= -deadZone || duty <= deadZone){
        // BRAKE
        digitalWrite(in_1,HIGH);
        digitalWrite(in_2,HIGH); 
    }  
}

```

**Servo Motor**
The servo motor has three wires coming out of it. One is power and one is ground, and the third is the control line that will receive information from the Arduino.
When a servo motor starts to move, it draws more current than if it were already in motion. This will cause a dip in the voltage on your board. By placing a 100uf capacitor across power and ground right next to the male header i can smooth out any voltage changes that may occur.
I can also place a capacitor across the power and ground going into my potentiometer.
These are called *decoupling capacitors* because they reduce or decouple, changes caused by the components from the rest of the circuit. 
Connecting the cathode to ground and anode to power.
If I put the capacitors in backwards, they can explode!

![Screenshot](../img/week4/servoMotor1212.jpg)

```
#include <Servo.h>
Servo myServo;
int const potPin = A0;
int potVal;
int angle;
void setup(){
  myServo.attach(9);
  Serial.begin(9600);
}
void loop(){
  potVal=analogRead(potPin);
  Serial.print("potVal:");
  Serial.print (potVal);
  angle=map(potVal,0,1023,0,179);
  Serial.print(",angle:");
  Serial.println(angle);
  //analogWrite(9, map(potVal,0,1023,0,255));
  myServo.write(angle);
  delay(15);
}
```

** The Stepper Motor**

Stepper motors are motors that have multiple coils in them, so that they can be moved in small increments or steps. Stepper motors are typically either unipolar or bipolar, meaning that they have either one main power connection or two. Whether a stepper is unipolar or bipolar,it can be controlled with a H-bridge.
There is no stepper motor on tinkercad, so we are using 3 led light to see the blinking steps.
Build the circuit:
•	Connect power and ground on the breadboard to power and ground from the microcontroller. Use 5V and a ground on th Arduino.
•	Place a L293D H-bridge on the breadboard. Stepper motors has 2 coils, so it is like driving to motors with the H-bridge.
•	Connect pin 8 to input 1
•	Connect pin 9 to input 2
•	Connect pin 10 to input 4
•	Connect pin 11 to input 3
•	connect 4 leds to output 1,2,3,4
•	Use 220 ohm resistor for all the leds
•	Use a potentiometer and connect the wiper pin to A0

![Screenshot](../img/week4/StepperMotor.jpg)

```
#include <Stepper.h>

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
                                     // for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8,9,10,11);

int stepcount=0; //the number of steps the motor has taken

void setup() {
  // initialize the serial port:
  Serial.begin(9600);
}

void loop() {
  Serial.print("steps:");
  Serial.printIn(stepCount);

  int stepDel= map(analogread(A0),1023,-500,500);
  if(stepDel >5){
  myStepper.step(1);
  stepCount++; //stepCount=stepCount+1
  }

  if(stepDel >-5){
  myStepper.step(-1)
  stepCount++; //stepCount=stepCount-1
  
```

![Screenshot](../img/week4/blinkStepByStep.jpg)
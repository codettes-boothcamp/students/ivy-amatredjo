**Electronics Production**

This week I learned to work with the following software KICAD, FlatCam. I have import and append libaries into KiCad.

**Working with KiCad**

I have first make a new folder in my local directory where I save all my work.

![Screenshot](../img/week9/kicadfolder.jpg)

And then made my first project, by selecting File-New-Project

![Screenshot](../img/week9/kicadopenproject.jpg)

I placed various components on my workspace by selecting de components with its Footprint.
The *Footprint gives the exact dimension*. If there are components missing, We have to import the libaries.
For my first project I have made an Adruino uno, using the Atmega328p-Au as the processor.
To relocate the component, click on the component itself and not on the name of the components.
Working with different componets (symbols) you will need the datasheet to see the specs of it. So you can see the pins, on which you can see what to connect.

![Screenshot](../img/week9/datasheetatmega.jpg)

Placing the components using the 3rd icon on the right. If the window opens you may search for the component that you needed.
After placing the componets (symbols), label them correctly.  
To rotate a component, seklect it first and then press "r".

![Screenshot](../img/week9/kicadchoosesymbol.jpg)

After placing all the symbols on the workspace, place the wires (green icon on the right) between the symbols to complete your KiCad Sketch.
If you finished with the sketch *Annotate* the sketch by clicking on Tools-Annotate Schematic.
Annotate is needed to see if your compoents or symbols are labeled correctly ( No double naming).

![Screenshot](../img/week9/kicadschematic.jpg)

![Screenshot](../img/week9/kicadannotate.jpg)

The next step is to *assign the footprint*, go to Tools-Assign Footprints,
before assigning the footprint, add the *fab mod* libary from your local drive and then import the libary.

![Screenshot](../img/week9/kicadassignfootprint.jpg)

![Screenshot](../img/week9/kicadfabmod.jpg)

![Screenshot](../img/week9/kicadassignfootprint2.jpg)

After we assign the footprint, we *generate the netlist*. Before generating the netlist, make a special folder in your local project folder. All netlists will be save in this folder.
To organise your project, place all netlist in your netlist folder on your local machine.
To generate the netlist click on the "Generate netlist" button on the toolbar.

![Screenshot](../img/week9/kicadreadnetlist.jpg)

![Screenshot](../img/week9/kicadreadingnetlist.jpg)

![Screenshot](../img/week9/kicadsavenetlist.jpg)

Run the PCB New (Printed Circuit Board) button on the toolbar.
You will get to see your PCB with all the components. Try to organize your components by placing the route tracks with the 5th icon on the right.
Route Tracks are colored in RED.

First organize your components:

![Screenshot](../img/week9/orgacomponents.jpg)

![Screenshot](../img/week9/pcb1.jpg)

Click on Setup-Design Rules to place the right Net Classes for the Clearance, Track Width and other things where needed.

![Screenshot](../img/week9/kicaddesignrules.jpg)

After placing my Route tracks, I place the Auxiliary Axus by clicking the *Drill and Place Offset* button in the tool panel or you should go to Place and click the same button.

![Screenshot](../img/week9/kicadplacedrill.jpg)

We can see the Dimension: 40.640 mm x 55.880 mm

KiCad Plot

After making the Route tracks, we plot the schematic by clicking File-Plot

![Screenshot](../img/week9/fcuplot.jpg)

![Screenshot](../img/week9/fcplothere.jpg)

**FlatCam**

Place all parameters and then save the file as an gbr-file

![Screenshot](../img/week9/fcgbrfile.jpg)

![Screenshot](../img/week9/flatcam1.jpg)

![Screenshot](../img/week9/fcparam.jpg)


Download links here

Reference

<a href="myFile.js" download>Click to Download</a>



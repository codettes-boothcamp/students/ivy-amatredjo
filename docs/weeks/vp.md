## Video Production

**Assignment:** 

- Practice what you learned today. 
- Search for tutorial videos and study how they assemble their videos.
- Create any content you want and display it Thursday. has to be a minimum of one minute


**Learning outcomes:** 

1. Explore and use Adobe Primiere Pro CC 2019 for Video Production
2. Using Ultra Key for the Green Screen
3. Have in mind that all the images and videos there you are using are put in a specific directory, because relocation mean errors!
4. Don't wear the colour green if you are working with a green screen
5. IF THERE SHOULD BE ELECTRIC EFFECT ON YOUR FOOTAGE, PLEASE SET YOUR MID POINT LOWER THEN 100 
6. Always use Audio for your transitions in a video 

**Assessment:** 

Have you:

- Day 1: Getting to know the most comman tools in Adobe Primiere
- Day 2: Making a Home Video of 1 min and edit it for presentation
- Day 3: Knowing the steps to make a better Story (Video) by filming with Sequence and working on a Better Audio with constant Power


**Week 2** was all about Video Production, we had too install Adobe Primiere CC2019 for this week assignment.
By creating a new Project, giving it a name and putting the Capture Format to HDV.

![Screenshot](../img/AP1311.jpg) 

![Screenshot](../img/newproject1811.jpg) 

Spotting the Imperfections on the footage we made with the greenscreen.
The green screen is a perfect colour to use for videos. Infact the colour *Blue* and *Green* are the colours that are the opposite of all the other colours.

**Some of these imperfections are:**

1. The distant between the object and the green screen (1 meter)
2. Lighting and no wrinkles
3. Position while recording, it's very important to have a fix position while recording
4. Use a 3.5 mm or higher microphone to have a reasonable audio

**- Importing file in Adobe Primiere **

![Screenshot](../img/import1311.jpg)

AP has different panels where you can work in.
After the file is imported, select the editing mode and select your Timeline Panel. Drag the imported file to your timeline and put some effect for it by 
working with the effect_control panel.

![Screenshot](../img/effcontrol1311.jpg)

**Important: don't forget to save your work after editing (CTRL+S)**

Steps for removing the green Screen on your footage:

1. Go to Effect, look for Ultra Key and drag it to your video on the timeline, you will see a purple *fx*
2. Go to the Source Panel and look for the effect Controls (to put effects on your video like position, snelheid)
3. At the Matte Generation, use Pedestal (most common: set to 100)
4. At the Matte Cleanup, use Contrast and Mid Point (most common: set to 100)

**- How to put a Background**

Import a background and drag it to your project. After it is in your project, drag into your timeline. Resize your background by going to the effect controls, motion
and then scale it by dragging. Or scale it from 120 or more.

Dont forget to put your new colour matte at 1920 x 1080

![Screenshot](../img/background1.jpg)

![Screenshot](../img/background2.jpg)

Use backspace to delete everything  you want from your workspace

**- Editing titles for your video**

Go to File, New and press on Legacy Title

Give your Title a name and use it for your video

*Remember, if you are using one title for many places in your video, after editing it, the change you made will come in every single title in your video.*

![Screenshot](../img/title1311.jpg)

**- Filming in sequence**

By filming in sequence (little steps), you will have a better story. A story should consist of min 3 steps.
Make *Dynamic* shots to keep viewers interested, no long boring shots and make them from Close to  Wide Shots.

When putting these shots together we can use video transitions on the effect panel between the various shots. By dragging it to the Timeline panel between the shots.

Some of the right effects for these steps or video transitions are: 

1. The Slide
2. Desolve

![Screenshot](../img/effect1811.jpg)

*Note: Always put audio for your transitions in your video*

**- Audio Editing**

For presentations select audio from 6 -12, by selecting your video and click on *Audio gain*, then adjust your gain by 4-5.
Music that are loud for presentations shoudld be set to <24, it can be in the range from 18-24.
Audio transitions shouls also have a *Constant Power* between them.

![Screenshot](../img/audio1811.jpg)


**- Exporting your Video**

Before exporting your video, be sure that you are on your timeline, select all the things you want to export by using the *I* or *O* (in and out) key on your keyboard.
Then go to File/Export/Media

![Screenshot](../img/io1811.jpg)

Put your Format settings on H.264 and your Preset to Youtube 720p HD and give your video a name on the Output Name field on the same screne.

![Screenshot](../img/format1811.jpg)

Save the Video on your directory. And your project has been saved as an MP4 video file.

![Screenshot](../img/exportdir1811.jpg)

![Screenshot](../img/exportdir21811.jpg)





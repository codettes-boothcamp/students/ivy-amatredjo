**RESEARCH 1**

*Working with the ESP32*

**Circuit Diagram**
Driving the speakers directly by connecting it to the ESP32 or Arduino will cause a lot of noise. 
So I will use an amplifier circuit to get a clear and loud sound. An LM393 based amplifier circuit is used, 
circuit diagram for the same is given below:

![Screenshot](../img/final/LM386AmplifierCircuit.jpg)

**Connecting Speaker to ESP32**
After making the above amplifier circuit, I connected the ESP32/Arduino to the amplifier circuit as shown below. 
Connected the digital pin 25/7 of ESP32/Arduino to 10K resistor and ground of the ESP32/Arduino to the ground of amplifier circuit.

![Screenshot](../img/final/CircuitDiagram.jpg)

**Programming ESP32 for Text To Speech**
First I have to install a library called as Talkie, where I downloaded from [here](https://github.com/bobh/ESP32Talkie). 
And then add it to Arduino IDE by going to Sketch->Include Library->Add .ZIP Library.
Then start writing the code by including all the required libraries. Wifi.h, WifiClient.h are used to create a client to connect to ISP using Wi-Fi. 
WebServer.h is used to create the web server and ESPmDNS.h is used for local mDNS request.

```
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Talkie.h>
```

Next define an object voice for Talkie.

```
Talkie voice;
```

**Defining the text**
You can also add more words/phonemes by recording the sound for each one and converting them into hex code. 
There are various softwares available for converting them into hex like Binary Viewer.
I have to look what will work best with my curcuit.

Define a function which can speak out any text. This function can be found in the Talkie library. 
This function uses simple logics. If it is zero then it speaks zero.

Now I have to set up the Wi-Fi. By entering and replacing the name and password of my Wi-Fi in the code. 
Because we are using HTTP protocol so we enter 80 in the server() function. 
Because 80 is the default port number for HTTP.

```
const char* ssid     = "aaaaaaa";
const char* password = "xxxx";
WebServer server ( 80 );
```

Now define an array htmlResponse to get the input from webpage.

```
char htmlResponse[3000];
```
**Making the webpage**
Next I define a function handleRoot() to create an html page. The snprintf() function is used to produce the html page coded in it. 
Now start coding the html page by passing the htmlResponse array to it with its size. Firstly give the heading named as “Text To Speech”. 
Then create the textbox and button in HTML.
Next write the JavaScript code. Firstly define a variable Number for the input we are getting and then assign the value entered in text box to the Number. 
At the end send the webpage to server.

```
void handleRoot() {
  snprintf ( htmlResponse, 3000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
          <h1>Text To Speech</h1>\
          <input type='text' name='msg' id='msg' size=7 autofocus> Number \
          <div>\
          <br><button id=\"speak_button\">Speak</button>\
          </div>\
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>\    
    <script>\
      var Number;\
      $('#speak_button').click(function(e){\
        e.preventDefault();\
        Number = $('#msg').val();\       
        $.get('/save?Number=' + Number , function(data){\
          console.log(data);\
        });\
      });\      
    </script>\
  </body>\
</html>"); 
   server.send ( 200, "text/html", htmlResponse );  
}
 

//In the next portion define a function handleSave(). Here we convert the string into integer, because the input we are 
getting is a string and to speak it out we have to use it as an integer.

void handleSave() {
  if (server.arg("Number")!= "")
  {
    Serial.println("Message: " + server.arg("Number"));
    String serverData = String(server.arg("Number"));
    int finalServer = serverData.toInt();
    sayNumber(finalServer);
    delay(2000);
  }
}
```

Now in setup() function, firstly, we define the digital pin 25 as output and keep it high. 
Then initialize the Wi-Fi using Wifi.begin() function and print some status messages. 
Then in next lines we start the server by calling the handleRoot() function and get the input from web page by calling the handleSave() function 
and finally print the message “HTTP server started”.

```
void setup() {
  pinMode(25, OUTPUT);
  digitalWrite(25, HIGH);
  delay(10);
  // Start serial
  Serial.begin(115200);
  delay(10);

  // Connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on ( "/", handleRoot );
  server.on ("/save", handleSave);

  server.begin();
  Serial.println ( "HTTP server started" );
}

```

[YOUTUBE Reference](https://youtu.be/43B6RtXiTkQ)



**Getting up and running connecting an ESP32 to the Watson IoT platform.**

The ESP32 has a faster and dual core processor, more memory, more I/O, and supports Bluetooth as well as WiFi.

To create my development environment I need to (1) get the Arduino IDE, (2) install the ESP32 extensions into the IDE, and also (3) add the MQTT library.

Adding the MQTT library. My sketch uses MQTT to communicate with the Watson IoT Platform, so I need to add the MQTT library to the Arduino IDE. This is using the PubSubClient by Nick O'Leary, its the standard Arduino MQTT library and works fine on the ESP32. To install it, in the Arduino IDE I goto the menu bar “Sketch” -> “Include Library” -> “Manage Libraries…” to bring up the Library Manager and then in the “Filter your search…” box I enter PubSub and THEN click “Install”.

Programming the ESP32

```
/**
 * Helloworld style, connect an ESP32 to IBM's Watson IoT Platform
 * 
 * Author: Anthony Elder
 * License: Apache License v2
 */
#include <SPI.h>
#include <WiFi.h>
#include <PubSubClient.h>

//-------- Customise these values -----------
const char* ssid = "<yourWifiSSID>";
const char* password = "<yourWifiPassword>";

#define ORG "quickstart" // your organization or "quickstart"
#define DEVICE_TYPE "esp32" // use this default for quickstart or customize to your registered device type
#define DEVICE_ID "test1" // use this default for quickstart or customize to your registered device id
#define TOKEN "<yourDeviceToken>" // your device token or not used with "quickstart"
//-------- Customise the above values --------

char server[] = ORG ".messaging.internetofthings.ibmcloud.com";
char topic[] = "iot-2/evt/status/fmt/json";
char authMethod[] = "use-token-auth";
char token[] = TOKEN;
char clientId[] = "d:" ORG ":" DEVICE_TYPE ":" DEVICE_ID;

WiFiClient wifiClient;
PubSubClient client(server, 1883, wifiClient);

void setup() {
 Serial.begin(115200); delay(1); Serial.println();

 initWiFi();
}

void loop() {

 if (!!!client.connected()) {
   Serial.print("Reconnecting client to "); Serial.println(server);
   while (!!!client.connect(clientId, authMethod, token)) {
     Serial.print(".");
     delay(500);
   }
   Serial.println();
 }

 String payload = "{ \"d\" : {\"counter\":";
 payload += millis()/1000;
 payload += "}}";
 
 Serial.print("Sending payload: "); Serial.println(payload);
 
 if (client.publish(topic, (char*) payload.c_str())) {
   Serial.println("Publish ok");
 } else {
   Serial.println("Publish failed");
 }

 delay(3000);
}

void initWiFi() {
 Serial.print("Connecting to "); Serial.print(ssid);
 if (strcmp (WiFi.SSID().c_str(), ssid) != 0) {
   WiFi.begin(ssid, password);
 }
 while (WiFi.status() != WL_CONNECTED) {
   delay(500);
   Serial.print(".");
 } 
 Serial.println(""); Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
}
```

**RESEARCH 2**

**Trying the RaspberryPi to speak out Text (Text To Speech or TTS)**

*Introduction: Making my Raspberry Pi Speak!*

I used a software package called Espeak to convert text (or strings) into spoken words, out loud, on my RaspberryPi. 

1. I modify the voice
2. I will Modify the voice for speed, language, and gender also (what's best for the Kid to understand)
3. There is a language choice

*Software*
I used python3 with my RaspberryPi.  

*Connections*
1. My Raspberry Pi is powered up and connected to my router
2. I Connect a speaker to my RaspberryPi

*Setting up the TTS (Text To Speech) Engine*
To make the Raspberry Pi speak and read some text aloud, I need a software interface to convert text to speech on the speakers. 
For this I need a Text To Speech engine. The TTS engine I am using for my Final Project is eSpeak.  
The voice may be a little robotic, however it can run offline.

First I tested if the audio is working on the Raspberry Pi.  Running the following command:

```
aplay /usr/share/sounds/alsa/*
```
I was able to hear the sounds like “Front Center”,”Front Left”, “Front Right” and so on, so my sound is working perfectly!  
For more volume type:

```
alsamixer
```

Next, I install eSpeak.  By typing the following in terminal:
I have downloaded eSpeak from this[link:](http://espeak.sourceforge.net/)

```
sudo apt-get install espeak
```
**Get the Raspberry Pi Speaking from the Command Line**

After eSpeak has been successfully installed on the Raspberry Pi, I run the following command to test eSpeak:

```
espeak "Text you wish to hear back" 2>/dev/null
```

**Using Python to Make Your Raspberry Pi Speak **
I use a Python program that speak words. I downloaded here [link:](https://github.com/DexterInd/Raspberry_Pi_Speech/blob/master/speak_text.py)
speak_text.py – This program reads aloud the text you enter and it also records the speech to a file named Text.wav and save this file in the desktop folder in the pi folder.

**Running the speak text Programs:**

I Connect my Pi to the internet.  
First I need to download and install the num2words Python package to convert the numbers to strings.  
This will convert an integer value to a string, which are then read aloud by the Raspberry Pi.  Use the following command:  

```
sudo pip3 install num2words
```

Then I go to the folder where I have copied the speak_text.py file and run the following command:

```
sudo python3 speak_text.py
```
When prompted, I enter some text I wish to hear back and then hear it back through my speakers or headphones.
This speak text code also records the voice to a file named Text.wav.

Then I run the following commands to hear back the recorded voice.

```
aplay /home/pi/Desktop/Text.wav
```
**Modifying the Voice**
he Espeak package provides a few good variations to the default voice used to speak out the text.  
he software supports more than 30 languages and a few of them are shown below.  The syntax is “-v<voice filename>”  
For example, we can use an American accent with “en-us”:

```
espeak -ven-us "espeak is awesome" 2>/dev/null
```

**Voices**

You can also specify a male voice, or a female voice.   
You can choose between a few different male voices :+m1,+m2,+m3,+m4,+m5,+m6,+m7.  
You can also use female voices: +f1,+f2,+f3,+f4. 

```
espeak -ven+f4 "espeak is awesome" 2>/dev/null
```

**Modifying Speech Speed**

The “-s” parameter controls the speed of reading a single word. It sets the speed in words-per-minute. 
The default value is 175. I generally use a faster speed of 260. 
The lower limit is 80. There is no upper limit, but about 500 is probably a practical maximum.  We can set the word speed like this:

Fast at 300 words per minute and slow at 80 words per minute

```
espeak -s300 "espeak is awesome" 2>/dev/null
```

**Pause Between Words**

We can use the “-g” to pause between words; the “g” stands for “word gap”. 
This option inserts a pause between words. The value is the length of the pause, in units of 10 ms (at the default speed of 170 wpm).
A slow pause with 100 ms between words.

```
espeak -g180 "espeak is awesome" 2>/dev/null
```

**RESEARCH 3**

*Looking for an user interface  using a Chromium Kiosk Method*

Speech to Text using a Web Speech API, with a speak_text.py program.
This program reads aloud the text you enter in the user interface.

I used Web Speech API for the Speech to Text- part. This specification defines a JavaScript API to enable web developers to incorporate speech 
recognition and synthesis into their web pages. It enables developers to use scripting to generate text-to-speech output and to use speech recognition 
as an input for forms, continuous dictation and control. The JavaScript API allows web pages to control activation and timing and to handle results 
and alternatives. 

```
<!DOCTYPE html>
<meta charset="utf-8">
<title>Speaking Panda Box</title>
<style>
  * {
    font-family: Verdana, Arial, sans-serif;
  }
  a:link {
    color:#000;
    text-decoration: none;
  }
  a:visited {
    color:#000;
  }
  a:hover {
    color:#33F;
  }
  .button {
    background: -webkit-linear-gradient(top,#008dfd 0,#0370ea 100%);
    border: 1px solid #076bd2;
    border-radius: 3px;
    color: #fff;
    display: none;
    font-size: 13px;
    font-weight: bold;
    line-height: 1.3;
    padding: 8px 25px;
    text-align: center;
    text-shadow: 1px 1px 1px #076bd2;
    letter-spacing: normal;
  }
  .center {
    padding: 10px;
    text-align: center;
  }
  .final {
    color: black;
    padding-right: 3px; 
  }
  .interim {
    color: gray;
  }
  .info {
    font-size: 14px;
    text-align: center;
    color: #777;
    display: none;
  }
  .right {
    float: right;
  }
  .sidebyside {
    display: inline-block;
    width: 45%;
    min-height: 40px;
    text-align: left;
    vertical-align: top;
  }
  #headline {
    font-size: 40px;
    font-weight: 300;
  }
  #info {
    font-size: 20px;
    text-align: center;
    color: #777;
    visibility: hidden;
  }
  #results {
    font-size: 14px;
    font-weight: bold;
    border: 1px solid #ddd;
    padding: 15px;
    text-align: left;
    min-height: 150px;
  }
  #start_button {
    border: 0;
    background-color:transparent;
    padding: 0;
  }
</style>
<h1 class="center" id="headline">
  <a href="https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo/weeks/finalproject/">
    Project Speaking Panda Box</a> First LOCAL TEST</h1>
<div id="info">
  <p id="info_start">Click on the microphone icon and begin speaking.</p>
  <p id="info_speak_now">Speak now.</p>
  <p id="info_no_speech">No speech was detected. You may need to adjust your
    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
      microphone settings</a>.</p>
  <p id="info_no_microphone" style="display:none">
    No microphone was found. Ensure that a microphone is installed and that
    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
    microphone settings</a> are configured correctly.</p>
  <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
  <p id="info_denied">Permission to use microphone was denied.</p>
  <p id="info_blocked">Permission to use microphone is blocked. To change,
    go to chrome://settings/contentExceptions#media-stream</p>
  <p id="info_upgrade">Web Speech API is not supported by this browser.
     Upgrade to <a href="//www.google.com/chrome">Chrome</a>
     version 25 or later.</p>
</div>
<div class="right">
  <button id="start_button" onclick="startButton(event)">
    <img id="start_img" src="mic.gif" alt="Start"></button>
</div>
<div id="results">
  <span id="final_span" class="final"></span>
  <span id="interim_span" class="interim"></span>
  <p>
</div>
<div class="center">
  <div class="sidebyside" style="text-align:right">
    <button id="copy_button" class="button" onclick="copyButton()">
      Copy and Paste</button>
    <div id="copy_info" class="info">
      Press Control-C to copy text.<br>(Command-C on Mac.)
    </div>
  </div>
  <div class="sidebyside">
    <button id="email_button" class="button" onclick="emailButton()">
      Create Email</button>
    <div id="email_info" class="info">
      Text sent to default email application.<br>
      (See chrome://settings/handlers to change.)
    </div>
  </div>
  <p>
  <div id="div_language">
    <select id="select_language" onchange="updateCountry()"></select>
    &nbsp;&nbsp;
    <select id="select_dialect"></select>
  </div>
</div>
<script>
var langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];

for (var i = 0; i < langs.length; i++) {
  select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 6;
updateCountry();
select_dialect.selectedIndex = 6;
showInfo('info_start');

function updateCountry() {
  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}

var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;

  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'mic-animate.gif';
  };

  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = 'mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = 'mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };

  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = 'mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
    if (create_email) {
      create_email = false;
      createEmail();
    }
  };

  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
    if (final_transcript || interim_transcript) {
      showButtons('inline-block');
    }
  };
}

function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

function createEmail() {
  var n = final_transcript.indexOf('\n');
  if (n < 0 || n >= 80) {
    n = 40 + final_transcript.substring(40).indexOf(' ');
  }
  var subject = encodeURI(final_transcript.substring(0, n));
  var body = encodeURI(final_transcript.substring(n + 1));
  window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
}

function copyButton() {
  if (recognizing) {
    recognizing = false;
    recognition.stop();
  }
  copy_button.style.display = 'none';
  copy_info.style.display = 'inline-block';
  showInfo('');
}

function emailButton() {
  if (recognizing) {
    create_email = true;
    recognizing = false;
    recognition.stop();
  } else {
    createEmail();
  }
  email_button.style.display = 'none';
  email_info.style.display = 'inline-block';
  showInfo('');
}

function startButton(event) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.lang = select_dialect.value;
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = 'mic-slash.gif';
  showInfo('info_allow');
  showButtons('none');
  start_timestamp = event.timeStamp;
}

function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}

var current_style;
function showButtons(style) {
  if (style == current_style) {
    return;
  }
  current_style = style;
  copy_button.style.display = style;
  email_button.style.display = style;
  copy_info.style.display = 'none';
  email_info.style.display = 'none';
}
</script>
```

**Research 4: eSpeak for nodejs for a Rasbian Lite version**

Espeak working with nodejs
This is a native binding of eSpeak for node. eSpeak is a compact open source software speech synthesizer for English and other languages.

Installing the package with:

```
npm install node-espeak
```
and then:

```
var ESpeak = require('node-espeak');
ESpeak.initialize();
ESpeak.onVoice(function(wav, samples, samplerate) {
    // TODO: Do something useful
});
ESpeak.speak("Hello world!");
```

**API**
```
initialize([voice, [properties]])
```
Must be called before eSpeak can be used, other wise an error will be thrown.
You may specify an object containing options for the voice to use. This will be passed to the setVoice() method.
You may also specify an object containing additional properties which will be passed to setProperties().

```
speak(text)
```
Will synthesize the given text. The synthesized audio will be passed to the function you specified with the onVoice() method.

```
onVoice(callback)
```
Will set the callback to call whenever speech was synthesized.

The callback should have three parameters:

1. data: The raw audio samples.
2. samplerate: The sample rate.
3. samples: The amount of samples generated.

```
getVoice()
```
Returns an object containing information about the voice eSpeak is using. The object will contain the following properties:

1. gender: "male", "female" or "none".
2. language: The two letter code of the language currently used.
3. age: The age of the voice.
4. variant: The variant of the voice eSpeak is using.

getProperties()
Returns an object containing the current properties of eSpeak. The object will contain the following properties:

1. pitch: The pitch. Values from 0 to 100.
2. volume: The current volume. Values from 0 to 200 or higher.
3. range: The range. 0 is totally monotonous, default is 50 and maximum is 100.
4. rate: The speed the voice will be played back in words per minute. Valus from 80 to 450.
5. gap: The gap between the words in 10ms (at normal rate).

```
setProperties(properties)
```
Will set the additional properties. This method takes an object as argument in the form of the object specified in getProperties(). 
You may also only specify some of the properties.

setVoice(voice)
Sets the voice to use. See the object defined in getVoice() for additional information about the object to pass to this method. You may also only specify some of the properties.

You can also set each property and option for the voice with one of the following seperate setters:

- setPitch(pitch)
- setVolume(volume)
- setRange(range)
- setRate(rate)
- setGap(gap)
- setAge(age)
- setLanguage(language)
- setVariant(variant)
- setGender(gender)

**Research 5**

Working with Chromium web browser:

```
sudo apt-get install chromium-browser
```

In order to have a better display you can also install MC core fonts using

```
sudo apt-get install ttf-mscorefonts-installer
```
Setting up a kiosk for the chronium webbrowser:

First I install some updates and also upgrade the Raspberry Pi, by using the following commands:

```
sudo aot-get update
```

and then

```
sudo aot-get upgrade
```
This will take a while, so go and make some coffee...



 
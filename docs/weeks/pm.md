## Project Managment

**Assignment:** 

- Build a personal site describing you and your final project.
- Upload it to the class archive.
- Work through a git tutorial.

**Learning outcomes:** 

1. Explore and use website development tools
2. Identify and utilise version control protocols

**Assessment:** 

Have you: 

- Made a website and described how you did it
- Introduced yourself
- Documented steps for uploading files to archive
- Pushed to the class archive


Tuesday, 5 November 2019 was the kickoff of codettes girls in ict innovation bootcamp and day 1 was training on Project Management. 
On that day I registered for both my  gitlab and github accounts.

[x] Register to Gitlab

![Screenshot](../img/gitlab0711.jpg) 

[x] Register to Github

![Screenshot](../img/github0711.jpg)

I  got access to my own repositories which I clone to my local machine using Github Desktop.
All my work will be saved in this directory on my local machine. 
This path is: C:\Users\scott\Documents\GitHub\ivy-amatredjo

[x] Clone to local machine

![Screenshot](../img/2clone0711.jpg)

[x] path to Clone on local machine

![Screenshot](../img/localclone0711.jpg)

It is very save to work Locally in Notepad ++ , which i had to install first.

[x] Working in Notepad++

 ![Screenshot](../img/notepad0711.jpg)
 
After you are done with editing your work you have to sync it with the githubdesktop by Pushing and Pulling it after commiting it to the master. 
In order to see the changes online.

All the images I put together must be save first in the right Folder, so knowing the right path was very important at all Times.
And this Path was on my machine: C:\Users\scott\Documents\GitHub\ivy-amatredjo\docs\img
There was a special folder for the images, which we should pull and push also via the Github Desktop. Have in mind that the size should not be so big, because we have a limitation of 10GB for this year.

[x] Commit to  master, then pull and push

![Screenshot](../img/committomaster0711.jpg)

![Screenshot](../img/pull0711.jpg)

![Screenshot](../img/push0711.jpg)

I was given an into the basic markdown syntax and requested to make simple changes to my local  repository such as adding basic text and images and then with the use of github desktop sync that changes to your hosted repository on gitlab. 
 
 [x] Github Desktop Autochange
 
![Screenshot](../img/pmmd0711.jpg)

The whole Process 

![Screenshot](../img/git0611.jpg)

I was also given my personal webpage url where I can preview my website online.
It was very quick and simple using just text in *Notepad++* to make changes in my webpage 

[Go To My Personal webpage](https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo)

![Screenshot](../img/website0711.jpg)







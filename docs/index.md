# About

![Screenshot](img/gezin0511.jpg)


**HI YOU GUYS!**

My Name is Ivy Amatredjo, Im 42 years old and live in *Domburg, SURINAME*.
Mother of 2. 
I used to work for the Suralco back then and now days I am running the Family Business with my Brother in the Horeca.
I Want to be a part of this Girls innovation Bootcamp because I want to show other WOMEN that age doesn't matter in the ICT.
We have great **Potential and Ideas** to show the world to make life easier and life is not all about Cooking and Feeding the babies at Home all the Time.
Nobody wants to be left behind nowdays with the TECH racing to the moon and Beyond.
AND *We are the dedicated ones who should guid our children for the Tech of Tommorow.*


## Final Project Concept

**Assignment:** Plan and sketch a potential final project

**Learning outcomes:** Communicate an initial project proposal

**Assessment:** 

Have you: 

- sketched your final project idea's 
- described what it will do and who will use it

**Background**

Dyslexia, also known as reading disorder, is characterized by trouble with reading despite normal intelligence. 
Different people are affected to varying degrees. Problems may include difficulties in spelling words, 
reading quickly, writing words, "sounding out" words in the head, pronouncing words when reading aloud and understanding what one reads

I want to do something to help my son read more. He has problems with sound (hearing, reading) and writing.
So it would be quit nice for him to have something that can help him read/write correctly. 
A book with sound that can correct the kid by itself would be very handy. 
It can also learn him to read/write difficult words too. 

*Some things that I maybe need List*	

1. Reading Pen/stick
2. Speakers
3. USB cable

*Unordered Options List*	

- Letters for making words
- Recordable stickers
- Selectable Language
- Downloadable stories


[x] first rough sketch of my final project

![Screenshot](img/fp0611.jpg)

Finding and Working on a better Design for my Final Project in week 6, after the christmas break.

![Screenshot](img/week5/4Panda14120.jpg)

[x] Looking for some ideas

[How to build a text to speech IoT Speaker](https://youtu.be/ZnKRj_AxD6k)

**Business Canvas for my Final Project**

[Link to my Business Canvas](https://canvanizer.com/canvas/rYWs1vHd7DhKN)

![Screenshot](img/week5/SPCanvas.PNG)

![Screenshot](img/week5/canvasA7120.jpg)

![Screenshot](img/week5/canvasB7120.jpg)

**My Hackomation Project: TRIBE**

Our project provides the resort or bussines with a platform of add-ons that include a dashboard with the view of visitor density and real-time location tracking. 
And the visitor gets an mobile app with an interactive map.

## Overview & Features
Our platform has the ability for add-ons like: 

- *Book*
- *Pay*
- *Free/busy*
- *Activity calender*

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them. 
For our project we are using google spreadsheet to store and collect data to build the interactive mobile app on Android and Apple devices. 
The environment that we are using is glide.

**Business Canvas for our Hackomation Project**

![Screenshot](img/week5/TRCanvas.PNG)

[Link to my Hackomation Project](https://hackomation1.gitlab.io/2020/hackers/innov8rs/)

## Who will use it?

**Goal: Help children with dyslexia, a reading disorder**

## What will it look like?
I Hope it will be a colourfull Toy-look loveable book/plate with a pen/stick attach on it. So children will be impressed just by looking at it. 
Wanting to try it hearing sound comming out of it.
It's like playing with a tab or a gameboard. So it's nicer then having a teacher or an angry mom besides or in front of them.


## What will it do?
The book or plate will have letters or words, just like a normal book with a pen/stick and when sliding over the words or letters it will make a sound.
Children who can not read a word of pronounce a word correctly will be helped by sliding the pen or stick over the word. So the child can read all by themself.
There will be an option to write words on your own or you can download your favorite story. 



